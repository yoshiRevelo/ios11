//
//  ViewController.swift
//  TaskManager
//
//  Created by Yoshi Revelo on 1/28/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var tasks = [NSManagedObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //1
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest : NSFetchRequest<Task> = Task.fetchRequest()
        print("Hola")
        
        //3
        do{
            let results = try managedContext.fetch(fetchRequest)
            tasks = results as [NSManagedObject]
            
        }catch(let error){
            print("Error al recuperar de CoreData \(error.localizedDescription)")
        }
        
        //4
        tableView.reloadData()
        
        //1: Exactamente igual a como hicimos en el método saveTask(), obtenemos nuestro managed object context a partir del appDelegate.
        
        //2: Creamos un objeto fetch request para recuperar nuestras tareas. Si recuerdas la teoría de NSFetchRequest que vimos al comienzo del tutorial, sabrás que tenemos que especificar una serie de calificadores para definir nuestra consulta. En este caso, hemos especificado que nos devuelva los objetos almacenados en la entidad Task. De esta forma, recuperaremos todas las tareas que haya almacenadas.
        
        //3: Una vez que hemos definido nuestra fetch request (La consulta que vamos a hacer a nuestra Base de Datos), la ejecutamos a través del método executeFetchRequest() y almacenamos los resultados en la variable results. Por último almacenamos estas tareas recuperadas en nuestro array tasks. Puedes ver, que seguimos controlando los errores utilizando un do-catch.
        
        //4: Por último actualizamos los datos de la tabla para que muestre por pantalla las tareas recuperadas.
    }

    //MARK: User Interaction
    @IBAction func addTaskButtonPressed(_ sender: Any) {
        //Creamos el UIAlertController
        let alert = UIAlertController(title: "Nueva Tarea", message: "Añade una nueva tarea", preferredStyle: .alert)
        
        //Creamos el UIAlertAction para guardar la tarea
        let saveAction = UIAlertAction(title: "Guardar", style: .default) { (action:UIAlertAction) in
            //Guardar la tarea y recargar el tableView
            let textField = alert.textFields?.first
            self.saveTask(nameTask: textField!.text!)
            self.tableView.reloadData()
        }
        //Creamos el UIAlertAction para cancelar
        let cancelAction = UIAlertAction(title: "Cancelar", style: .default) { (action:UIAlertAction) in
        }
        //Añadimos el TextField al UIAlertController
        alert.addTextField { (textField: UITextField) in
        }
        //Añadimos las alertas
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        //Lanzamos el UIALertController
        present(alert, animated: true, completion: nil)
    }
    
    //Private Methods
    private func saveTask(nameTask: String){
        //1
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //2
        let entity = NSEntityDescription.entity(forEntityName: "Task", in: managedContext)
        let task = NSManagedObject(entity: entity!,insertInto: managedContext)
        
        //3
        task.setValue(nameTask, forKey: "name")
        
        //4
        do{
            try managedContext.save()
            
            //5
            tasks.append(task)
        }catch(let error){
            print("Error al guardar CoreData \(error.localizedDescription)")
        }
        
        //1: Si recuerdas la parte de teoría que vimos, hemos mencionado, que para guardar datos en Core Data, lo primero que debemos hacer es guardarlos en nuestro managed object context. Por tanto, lo que hacemos es recuperar nuestro managed object context a partir del appDelegate.
        
        //2: Una vez que tenemos nuestro managed object context, creamos un objeto de tipo NSEntityDescription y lo almacenamos en la variable entity, que representa nuestra entidad Task. A partir de este objeto entity, creamos un objeto managed object y lo almacenamos en nuestro managed object context. (Recuerda que NSManagedObject representa un objeto único almacenado en Core Data. En este caso representa a nuestra tarea)
        
        //3: Utilizamos KVC para especificar el nombre de nuestra tarea. Ten en cuenta que hemos utilizado “name” porque cuando hemos creado el modelo, especificamos un atributo llamado “name”. Si tu le has dado un nombre diferente al crear ese atributo en el modelo, aquí tendrás que poner ese mismo nombre. Si no cumples esta condición, tu aplicación crasheará.
        
        //4: Utilizamos el método save() del managed object context para guardar nuestra tarea en disco. Como esta operación de guardado puede fallar y lanzar un error, realizamos la llamada dentro de un try-catch.
        
        //5: Una vez que hemos guardado nuestra tarea a través de Core Data, ya solo nos queda almacenarla en nuestro array tasks, para que se muestre cuando recarguemos la table view.
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        let task = tasks[indexPath.row]
        cell?.textLabel?.text = task.value(forKey: "name") as? String
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

