//
//  ViewController.swift
//  TocuhIDAuth
//
//  Created by Yoshi Revelo on 12/28/18.
//  Copyright © 2018 Yoshi Revelo. All rights reserved.
//

import UIKit
import LocalAuthentication

class ViewController: UIViewController {

    @IBOutlet weak var resultLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let authContext = LAContext()
        
        var error: NSError?
        
        if authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error){
            authContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "To Check if it's really you") { (success, error) in
                if success{
                    self.resultLabel.text = "Result:  Verified"
                    self.resultLabel.textColor = .green
                }else{
                    self.resultLabel.text = "Result:  Not Verified"
                    self.resultLabel.textColor = .red
                }
            }
        }
        
    }


}

