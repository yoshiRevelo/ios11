//
//  DetalleInterfaceController.swift
//  HolaMundo WatchKit Extension
//
//  Created by Yoshi Revelo on 3/11/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import WatchKit
import Foundation


class DetalleInterfaceController: WKInterfaceController {

    @IBOutlet weak var resultadoLabel: WKInterfaceLabel!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        let c = context as!  Resultado
        
        resultadoLabel.setText("\(c.imc)")
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
