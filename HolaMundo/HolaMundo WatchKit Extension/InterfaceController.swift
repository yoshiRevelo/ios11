//
//  InterfaceController.swift
//  HolaMundo WatchKit Extension
//
//  Created by Yoshi Revelo on 3/11/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    var pesoActual: Float = 1
    var estaturaActual: Float = 1
    
    @IBOutlet weak var holaMundoLabel: WKInterfaceLabel!
    
    @IBOutlet weak var pesoLabel: WKInterfaceLabel!
    
    @IBOutlet weak var alturaLabel: WKInterfaceLabel!
    
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    private func imc(peso: Float, estatura: Float) -> Float{
        let imc = peso/(estatura*estatura)
        return imc
    }

    @IBAction func pesoSliderValue(_ value: Float) {
        pesoActual = value
        pesoLabel.setText("\(value) kg")
    }
    
    @IBAction func alturaSliderValue(_ value: Float) {
        estaturaActual = value / 100
        alturaLabel.setText("\(estaturaActual) m")
    }
    
    @IBAction func saludarButtonPressed() {
        let resultado = imc(peso: pesoActual, estatura: estaturaActual)
        let valorContexto = Resultado(descripcion: "Peso Normal", imc: resultado)
        pushController(withName: "IdentificadorValor", context: valorContexto)
        
        
    }
    
    
}
