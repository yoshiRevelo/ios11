//
//  Resultado.swift
//  HolaMundo WatchKit Extension
//
//  Created by Yoshi Revelo on 3/11/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import WatchKit

class Resultado: NSObject {
    var descripcion: String = ""
    var imc: Float = 0
    
    init(descripcion: String, imc: Float){
        self.descripcion = descripcion
        self.imc = imc
    }
}
