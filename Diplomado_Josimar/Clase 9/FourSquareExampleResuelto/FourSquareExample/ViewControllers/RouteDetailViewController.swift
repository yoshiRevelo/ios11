//
//  RouteDetailViewController.swift
//  FourSquareExample
//
//  Created by MobileStudio Laptop003 on 09/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import MapKit

class RouteDetailViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var venueNameLabel: UILabel!
    @IBOutlet weak var instructionsLabel: UILabel!
    
    
    var annotation: MKAnnotation!
    var userAnnotation: MKAnnotation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        venueNameLabel.text = annotation.title ?? "Sin nombre"
        
        mapView.addAnnotation(userAnnotation)
        mapView.addAnnotation(annotation)
        
        //Para acercar los puntos entre el arreglo de annotations enviados
        mapView.showAnnotations([annotation, userAnnotation], animated: true)
        
        loadRoute()
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - private methods
    
    /*private func clearRoutes(){
        let currentOverlays = mapView.overlays
        mapView.removeOverlays(currentOverlays)
    }*/
    
    private func loadRoute(){
        
        //clearRoutes()
        
        //MapItem a través de un placeMark a través de coordenadas
        let placeMark = MKPlacemark(coordinate: annotation.coordinate)
        let destinationMapItem = MKMapItem(placemark: placeMark)
        
        let request = MKDirectionsRequest()
        request.transportType = .walking
        request.requestsAlternateRoutes = false
        request.source = MKMapItem.forCurrentLocation()
        request.destination = destinationMapItem
        
        let directions = MKDirections(request: request)
        
        directions.calculate { (response, error) in
            if let error = error{
                print("Directions error: \(error.localizedDescription)")
            }
            else{
                if let routes = response?.routes{
                    for route in routes{
                        var finalStepsStr = ""
                        for step in route.steps{
                            print(step.instructions)
                            if !step.instructions.isEmpty{
                                finalStepsStr += "\(step.instructions)\n"
                            }
                        }
                        self.instructionsLabel.text = finalStepsStr
                        self.mapView.add(route.polyline, level: .aboveRoads)
                    }
                }
            }
        }
    }

}

//MARK: - CLLocationManagerDelegate


//MARK: - MKMapViewDelegate
extension RouteDetailViewController: MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = .red
        renderer.lineWidth = 10
        renderer.alpha = 0.5
        return renderer
    }
}

