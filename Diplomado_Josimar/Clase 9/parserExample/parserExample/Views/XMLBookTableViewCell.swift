//
//  XMLBookTableViewCell.swift
//  parserExample
//
//  Created by MobileStudio Laptop004 on 26/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
protocol XMLCellDelegate: class {
    func xmlCellDidShare(book: Book)
}

class XMLBookTableViewCell: UITableViewCell {

    
    weak var delegate: XMLCellDelegate?
    
    //MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    
    var book:Book!{
        didSet{
            configureCell()
        }
    }
    
    //MARK: - override nativeMethods
    override func awakeFromNib() {
        super.awakeFromNib()
        shareButton.layer.cornerRadius = shareButton.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func shareButtonPressed(_ sender: Any) {
        print("Share book: \(book.title)")
        delegate?.xmlCellDidShare(book: book)
    }
    //MARK: - Private Methods
    
    func configureCell(){
        titleLabel.text = book.title
        authorLabel.text = book.author
        priceLabel.text = "$\(book.price)"
    }
}
