//
//  PhotoImageModel.swift
//  CollectionViewExample
//
//  Created by MobileStudio Laptop004 on 30/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

class PhotoImageModel: Codable{
    var height: String
    var width: String
    var path: String
    var format: String
}
