//
//  ProductDataModel.swift
//  CollectionViewExample
//
//  Created by MobileStudio Laptop004 on 30/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

class ProductDataModel: Codable {
    var description: String
    var price: String
    var sku: String
    var url: String
    var name: String
    var brand: String
}

extension ProductDataModel{
    var formattedPrice: String{
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.currencySymbol = "$"
        numberFormatter.currencyDecimalSeparator = "."
        numberFormatter.currencyGroupingSeparator = ","
        
        if let doublePrice =  Double(price){
            let numberPrice = NSNumber(value: doublePrice)
            return numberFormatter.string(from: numberPrice) ?? "-"
        }
        return "-"
    }
}
