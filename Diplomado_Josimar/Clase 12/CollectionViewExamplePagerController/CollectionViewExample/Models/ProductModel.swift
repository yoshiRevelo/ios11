//
//  ProductModel.swift
//  CollectionViewExample
//
//  Created by MobileStudio Laptop004 on 30/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

class ProductModel: Codable{
    var id: String
    var data: ProductDataModel
    var images: [PhotoImageModel]
}
