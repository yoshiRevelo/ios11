//
//  Constants.swift
//  CollectionViewExample
//
//  Created by Ricardo López on 23/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

struct Constants {
    
    struct API {
        static let productsUrl = "https://www.mobilestudio.mx/iphone/products/products.json"
    }
}
