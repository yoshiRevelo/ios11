//
//  ListCollectionViewCell.swift
//  CollectionViewExample
//
//  Created by MobileStudio Laptop004 on 30/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import AlamofireImage

class ListCollectionViewCell: UICollectionViewCell {
 
    //MARK: - Outlets
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productBrandLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    
    var product: ProductModel!{
        didSet{
            configureCell()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        productImageView.image = nil
        productBrandLabel.text = ""
        productPriceLabel.text = ""
        productNameLabel.text = ""
    }
    
    //MARK: - private methods
    
    private func configureCell(){
        productNameLabel.text = product.data.name
        productPriceLabel.text = product.data.formattedPrice
        productBrandLabel.text = product.data.brand
        
        if let firstImage = product.images.first, let imageURL = URL(string: firstImage.path){
            productImageView.af_setImage(withURL: imageURL, imageTransition: UIImageView.ImageTransition.crossDissolve(0.25))
        }
    }
}
