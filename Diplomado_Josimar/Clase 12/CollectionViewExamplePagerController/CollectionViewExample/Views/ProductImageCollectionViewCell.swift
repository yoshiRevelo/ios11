//
//  ProductImageCollectionViewCell.swift
//  CollectionViewExample
//
//  Created by MobileStudio Laptop004 on 30/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import AlamofireImage

class ProductImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    var photoImage: PhotoImageModel!{
        didSet{
            configureCell()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        photoImageView.image = nil
    }
    //MARK: - private methods
    
    private func configureCell(){
        if let imageURL = URL(string: photoImage.path){
            photoImageView.af_setImage(withURL: imageURL, imageTransition: UIImageView.ImageTransition.crossDissolve(0.25))
        }
    }
}
