//
//  GridCollectionViewCell.swift
//  CollectionViewExample
//
//  Created by MobileStudio Laptop004 on 30/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import AlamofireImage

class GridCollectionViewCell: UICollectionViewCell {
 
    //MARK: - Outlets
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameProductLabel: UILabel!
    
    var product: ProductModel!{
        didSet{
            configureCell()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        productImageView.image = nil
        nameProductLabel.text = ""
    }
    
    //MARK: - Private Methods
    
    private func configureCell(){
        nameProductLabel.text = product.data.name
        
        if let firstImage = product.images.first, let imageURL = URL(string: firstImage.path){
            productImageView.af_setImage(withURL: imageURL, imageTransition: UIImageView.ImageTransition.crossDissolve(0.25))
        }
        
    }
}
