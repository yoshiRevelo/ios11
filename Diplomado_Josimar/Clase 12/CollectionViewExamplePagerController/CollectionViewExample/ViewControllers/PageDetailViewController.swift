//
//  PageDetailViewController.swift
//  CollectionViewExample
//
//  Created by MobileStudio Laptop004 on 30/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class PageDetailViewController: UIViewController {

    private var pageViewController: UIPageViewController!
    var currentIndex = 0
    var datasource: [ProductModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pageViewController = storyboard?.instantiateViewController(withIdentifier: "PageViewController") as? UIPageViewController
        pageViewController.dataSource = self
        
        addChild(pageViewController)
        view.addSubview(pageViewController.view)
        pageViewController.didMove(toParent: self)
        
        let firstViewController = createviewControllerAt(index: currentIndex)
        //let firstViewController = UIViewController()
        //firstViewController.view.backgroundColor = .brown
        pageViewController.setViewControllers([firstViewController], direction: .forward, animated: false, completion: nil)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    //MARK: - private methods
    
    private func createviewControllerAt(index: Int)-> UIViewController{
        let productDetailViewController = storyboard?.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        productDetailViewController.product = datasource[index]
        productDetailViewController.pageIndex = index
        return productDetailViewController
    }
}


//MARK: - UIPageViewControllerDataSource

extension PageDetailViewController: UIPageViewControllerDataSource{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let currentProductDetailViewController = viewController as? ProductDetailViewController{
            let currentPageIndex = currentProductDetailViewController.pageIndex
            if currentPageIndex == 0 {
                return nil
            }
            let productDetailViewController = createviewControllerAt(index: currentPageIndex - 1)
            return productDetailViewController
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let currentProductDetailViewController = viewController as? ProductDetailViewController{
            let currentPageIndex = currentProductDetailViewController.pageIndex
            if currentPageIndex == datasource.count - 1 {
                return nil
            }
            let productDetailViewController = createviewControllerAt(index: currentPageIndex + 1)
            return productDetailViewController
        }
        return nil
    }
}
