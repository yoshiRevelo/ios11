//
//  ProductDetailViewController.swift
//  CollectionViewExample
//
//  Created by MobileStudio Laptop004 on 30/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ProductDetailViewController: UIViewController {
    
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productDescriptionLabel: UILabel!
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    @IBOutlet weak var imagePageController: UIPageControl!
    
    var product: ProductModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Producto recibido: \(product.data.name)")
        productNameLabel.text = product.data.name
        productDescriptionLabel.text = product.data.description
        productPriceLabel.text = product.data.formattedPrice
        imagePageController.numberOfPages = product.images.count
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: - UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
extension ProductDetailViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return product.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductImageCollectionViewCell", for: indexPath) as! ProductImageCollectionViewCell
        cell.photoImage = product.images[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Open full screen gallery")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //tamaño del collection View
        return collectionView.frame.size
    }
}


//MARK: - UIScrollViewDelegate

extension ProductDetailViewController: UIScrollViewDelegate{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView is UICollectionView{
            //offset espacio que se salió del controlador principal
            let currentIndex = scrollView.contentOffset.x / galleryCollectionView.frame.width
            imagePageController.currentPage = Int(currentIndex)
        }
    }
}
