//
//  ProductListViewController.swift
//  CollectionViewExample
//
//  Created by Ricardo López on 23/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import Alamofire

class ProductListViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    private var isGrid = false
    private var dataSource: [ProductModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadProducts()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ProductDetailViewController"{
            let productDetailViewController = segue.destination as! ProductDetailViewController
            productDetailViewController.product = sender as? ProductModel
        }
    }
    
    // MARK: - User interaction
    
    @IBAction func segmentedChaged(_ sender: UISegmentedControl) {
        print(sender.selectedSegmentIndex)
        if sender.selectedSegmentIndex == 1 {
            isGrid = true
        }
        else {
            isGrid = false
        }
        collectionView.reloadData()
    }
    
    // MARK: - Private methods
    
    private func loadProducts() {
        
        Alamofire.request(Constants.API.productsUrl)
            .validate()
            .responseData { (response) in
            
                switch response.result {
                case .success(let value):
                    print("Products JSON: \(value)")
                    
                    do{
                        let productsResponse = try JSONDecoder().decode(ProductsResponseModel.self, from: value)
                        print("Productos Parseados: \(productsResponse.metadata.results.count)")
                        self.dataSource = productsResponse.metadata.results
                        self.collectionView.reloadData()
                    }catch let error{
                        print("Parsing error: \(error.localizedDescription)")
                    }
                case .failure(let error):
                    print("Reques error: \(error.localizedDescription)")
                }
        }
    }
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegate

extension ProductListViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if isGrid {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GridCollectionViewCell", for: indexPath) as! GridCollectionViewCell
            cell.product = dataSource[indexPath.item]
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListCollectionViewCell", for: indexPath) as! ListCollectionViewCell
            cell.product = dataSource[indexPath.item]
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Selected: \(indexPath.item)")
        let product = dataSource[indexPath.item]
        performSegue(withIdentifier: "ProductDetailViewController", sender: product)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenWidth = view.frame.width
        
        if isGrid {
            let cellWidth = (screenWidth - 30) / 2
            let size = CGSize(width: cellWidth, height: cellWidth)
            return size
        }
        else {
            let cellWidth = screenWidth - 20
            let size = CGSize(width: cellWidth, height: 120)
            return size

        }
    }
}













