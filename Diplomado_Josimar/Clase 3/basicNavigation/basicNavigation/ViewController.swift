//
//  ViewController.swift
//  basicNavigation
//
//  Created by MobileStudio Laptop005 on 28/04/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - User Interaction
    
    @IBAction func showViewControllerProgramaticallyButtonPressed(_ sender: Any) {
        
        //1)debemos conectar el segue al viewController y no al botón
        // 2) debemos asignar un identificador al segue para poder ejecutarlo desde código
        //3) Mandamos llamar el segue
        performSegue(withIdentifier: "redViewController", sender: self)
    }
    
    @IBAction func showViewControllerWithIdButtonPressed(_ sender: Any) {
        //1) debemos asignar un identificador al viewController
        //2) debemos crear el viewController desde el storyBoard para poder presentarlo
        //3) presentamos el viewController a través del navigationController
        
        if let grayViewController = storyboard?.instantiateViewController(withIdentifier: "grayViewController"){
            navigationController?.show(grayViewController, sender: self)
        }
        
        //Para pasar a un viewController que está en otro StoryBoard desde código.
        /*let secondStoryBoard = UIStoryboard(name: "Second", bundle: nil)
        let blueViewController = secondStoryBoard.instantiateViewController(withIdentifier: "blueViewController")
        navigationController?.show(blueViewController, sender: nil)*/
    }
    
    //1) se agrega un StoryBoard
    //2) Se agrega viewController
    //3) Se asigna ID al viewController al que queremos llegardesde el primer StoryBoard
    //4) se agrega un StoryBoardReference en el primer StoryBoard
    //5) se le indica en el StoryBoardReference a que StoryBoard y a qu ID de viewController hace referencia
    
}

