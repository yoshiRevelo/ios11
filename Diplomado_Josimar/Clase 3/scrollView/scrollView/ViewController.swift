//
//  ViewController.swift
//  scrollView
//
//  Created by MobileStudio Laptop005 on 28/04/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var contentScrollView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let pages = contentScrollView.frame.size.width/view.frame.width
        pageControl.numberOfPages = Int(pages)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK: - Extensions
extension ViewController: UIScrollViewDelegate{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //contentOffset
        // distancia entre donde inicia el scroll hacia
        // donde esta el nuevo contenido
        let currentPage = scrollView.contentOffset.x/view.frame.width
        pageControl.currentPage = Int(currentPage)
    }
}

