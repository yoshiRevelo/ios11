//
//  NewsDetailViewController.swift
//  scrollView
//
//  Created by MobileStudio Laptop005 on 28/04/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class NewsDetailViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = "Crean mochila 'Spallbags' capaz de darle un descanso a la espalda"
        dateLabel.text = "28/04/2018"
        textLabel.text = "CIUDAD DE MÉXICO \n\nPara los estudiantes de cualquier nivel educativo es menester llevar consigo una mochila o maleta en la cual se puedan depositar y transportar los diversos materiales de estudio y libros que se necesitan consultar. \n\nSin embargo, esa actividad, tan común entre los alumnos, puede traer consigo un sinfín de lesiones en los hombros, el cuello y la espalda, debido a las gran cantidad de peso que se concentra en una específica parte del cuerpo."
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
