//
//  VisualFormatLanguageViewController.swift
//  scrollView
//
//  Created by MobileStudio Laptop005 on 28/04/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class VisualFormatLanguageViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var contentScrollView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //let redView = UIView(frame: CGRect(x: 0, y: 0, width: 1000, height: 1000))
        /*let redView = UIView()
        redView.backgroundColor = .red
        contentScrollView.addSubview(redView)
        
        //no trasnsforme los frames a constraints
        redView.translatesAutoresizingMaskIntoConstraints = false
        
        //Definir el formato 
        let horizontalFormat = "H:|-[redView]-|"
        let verticalFormat = "V:|-[redView(1000)]-|"
        
        let views = [
            "redView": redView
        ]
        
        let horizontalContraints = NSLayoutConstraint.constraints(withVisualFormat: horizontalFormat, options: .alignAllFirstBaseline, metrics: nil, views: views)
        
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: verticalFormat, options: .alignAllFirstBaseline, metrics: nil, views: views)
        
        contentScrollView.addConstraints(horizontalContraints)
        contentScrollView.addConstraints(verticalConstraints) */
        // Do any additional setup after loading the view.
        createViews(numberOfViews: 10)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Private methods
    
    private func createViews(numberOfViews: Int){
        
        var verticalViews : [String : UIView] = [:]
        var verticalElements : [String] = []
        for x in 0..<numberOfViews{
            //Crear view y lo ponemos en el scrollView
            let view = UIView()
            view.backgroundColor = .orange
            view.translatesAutoresizingMaskIntoConstraints = false
            contentScrollView.addSubview(view)
            
            //creamos un identificador único para el view
            let viewString = "view\(x)"
            
            // Agregamos el identificador al arreglo de Elementos
            verticalElements.append("[\(viewString)(altura)]")
            
            // Agregamos el view al diccionario para referencia en las containtss
            verticalViews[viewString] = view
            
            //Creamos el formato de constraints horizontal
            let horizontalFormat = "H:|-[\(viewString)]-|"
            
            //agregamos las constraints horizontales
            let horizontalContraints = NSLayoutConstraint.constraints(withVisualFormat: horizontalFormat, options: .alignAllFirstBaseline, metrics: nil, views: verticalViews)
            
            //Agregamos las constraints al scrollView
            contentScrollView.addConstraints(horizontalContraints)
        }
        
        print(verticalElements)
        // Creamos el formato de constraints vertical
        let verticalFormat = "V:|-\(verticalElements.joined(separator: "-"))-|"
        
        let metrics = [
            "altura" : 100
        ]
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: verticalFormat, options: .alignAllLeft , metrics: metrics, views: verticalViews)
        contentScrollView.addConstraints(verticalConstraints)
        print(verticalFormat)
    }
}



