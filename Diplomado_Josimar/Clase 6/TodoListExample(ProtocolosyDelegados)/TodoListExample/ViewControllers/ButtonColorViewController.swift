//
//  ButtonColorViewController.swift
//  TodoListExample
//
//  Created by MobileStudio Laptop004 on 19/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

protocol ChangeButtonColor: class {
    func selectColorPressed(sender: UIButton)
}

class ButtonColorViewController: UIViewController {

    weak var delegate: ChangeButtonColor?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK: -User Interaction
    
    @IBAction func changeColorButtonPressed(_ sender: UIButton) {
        let backgroundButtonColor = sender
        delegate?.selectColorPressed(sender: backgroundButtonColor)
    }
}
