//
//  ConfigurationViewController.swift
//  TodoListExample
//
//  Created by MobileStudio Laptop004 on 12/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ConfigurationViewController: UIViewController {

    //MARK: - Outlets
    
    @IBOutlet weak var detailSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let isDetailActive = UserDefaults.standard.bool(forKey: "DETAIL_ACTIVE")
        detailSwitch.setOn(isDetailActive, animated: false)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - User Interaction
    
    @IBAction func detailSwitchChanged(_ sender: Any) {
        print("cambia")
        
        UserDefaults.standard.set(detailSwitch.isOn, forKey: "DETAIL_ACTIVE")
        UserDefaults.standard.synchronize()
    }
    
}
