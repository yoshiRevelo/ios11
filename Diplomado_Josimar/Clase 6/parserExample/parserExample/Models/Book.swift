//
//  Book.swift
//  parserExample
//
//  Created by MobileStudio Laptop004 on 19/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

//Codable -> JSON

//BookModel
class Book: Codable {
    var id = ""
    var author = ""
    var title = ""
    var genre = ""
    var price = ""
    var publishDate = ""
    var bookDescription = ""
    
    // para que la clase obtenga los datos desde el JSON, es necesario declararlos en el enum aunque no cambie la llave o en caso contrario, si las llaves coinciden al 100% NO declaramos el enum
    enum CodingKeys: String, CodingKey{
        case id
        case author
        case title
        case genre
        case price
        case publishDate = "publish_date"
        case bookDescription = "description"
    }
}
