//
//  Constants.swift
//  parserExample
//
//  Created by MobileStudio Laptop004 on 19/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation
struct Constants {
    struct API {
        static let xmlURLStr = "https://www.mobilestudio.mx/iphone/parser/books.xml"
        static let jsonURLStr = "https://www.mobilestudio.mx/iphone/parser/books.json"
    }
}
