//
//  JSONParserViewController.swift
//  parserExample
//
//  Created by MobileStudio Laptop004 on 19/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import Alamofire

class JSONParserViewController: UIViewController {

    //MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    private var datasource: [Book] = []
    private var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "JSON Parser"
        refreshControl.addTarget(self, action: #selector(loadBooks), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        loadBooks()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "BookDetailViewController" {
            let bookDetailViewController = segue.destination as! BookDetailViewController
            bookDetailViewController.book = sender as? Book
        }
    }
 

    //MARK: - Private Methods
    
    @objc private func loadBooks(){
        Alamofire.request(Constants.API.jsonURLStr)
            .validate()
            .responseData { (response) in
                switch response.result{
                case .success(let value):
                    self.datasource.removeAll()
                    print("JSONData: \(value)")
                    do{
                        let booksResponse = try JSONDecoder().decode(BooksResponeModel.self , from: value)
                        print("Libros parseados: \(booksResponse.catalog.books.count)")
                        self.datasource = booksResponse.catalog.books
                        self.tableView.reloadData()
                        self.refreshControl.endRefreshing()
                        //TODO: Implemenetar Tabla
                    }catch let error{
                        print("\(error.localizedDescription)")
                    }
                case .failure(let error):
                    print("Error \(error.localizedDescription)")
                }
        }
    }
}

//MARK: - UITableViewDataSource, UITableViewDelegate

extension JSONParserViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "JSONBookTableViewCell") as! JSONBookTableViewCell
        cell.book = self.datasource[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let book = datasource[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "BookDetailViewController", sender: book)
    }
}
