//
//  XMLParserViewController.swift
//  parserExample
//
//  Created by MobileStudio Laptop004 on 19/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import Alamofire

class XMLParserViewController: UIViewController {
    
    private var auxBook: Book!
    private var auxString: String!
    private var dataSource: [Book] = []
    private var refreshControl = UIRefreshControl()
    
    //MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - override methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "XML Parser"
        //self -> ViewController
        
        refreshControl.addTarget(self, action: #selector(loadBooks), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        loadBooks()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "BookDetailViewController" {
            let bookDetailViewController = segue.destination as! BookDetailViewController
            bookDetailViewController.book = sender as? Book
        }
    }
    
    //MARK: - Private Methods
    
    @objc private func loadBooks(){
        Alamofire.request(Constants.API.xmlURLStr)
            // validar que el codigo de respuesta sea valido 200 - 299
        .validate()
            .responseData { (response) in
                switch response.result {
                case .success(let value):
                    
                    self.dataSource.removeAll()
                    print("Data: \(value)")
                    let parser = XMLParser(data: value)
                    parser.delegate = self
                    parser.parse()
                    
                    for book in self.dataSource{
                        print(book.title)
                        print("\t\(book.id)")
                        print("\t\(book.author)")
                        print("\t\(book.title)")
                        print("\t\(book.genre)")
                        print("\t\(book.bookDescription)")
                        print("\t\(book.price)")
                        print("\t\(book.publishDate)")
                    }
                    print("Libros Parseados: \(self.dataSource.count)" )
                    
                    self.tableView.reloadData()
                    
                    self.refreshControl.endRefreshing()
                case .failure(let error):
                    print("XML Error: \(error.localizedDescription)")
                }
        }
    }
}

//MARK: - XMLParserDelegate

extension XMLParserViewController: XMLParserDelegate{
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        //print("Abre: \(elementName)")
        if elementName == "book"{
            auxBook = Book()
            if let bookId = attributeDict["id"]{
                auxBook.id = bookId
            }
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        //print("Caracteres encontrados...")
        auxString = string
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        //print("Cierra: \(elementName)")
        switch elementName {
        case "author":
            auxBook.author = auxString
        case "genre":
            auxBook.genre = auxString
        case "title":
            auxBook.title = auxString
        case "price":
            auxBook.price = auxString
        case "publish_date":
            auxBook.publishDate = auxString
        case "description":
            auxBook.bookDescription = auxString
        case "book":
            dataSource.append(auxBook)
        default:
            print("Nothing to do here 😢")
        }
    }
}

extension XMLParserViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "XMLBookTableViewCell") as! XMLBookTableViewCell
        cell.book = dataSource[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let book = dataSource[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "BookDetailViewController", sender: book)
    }
}
