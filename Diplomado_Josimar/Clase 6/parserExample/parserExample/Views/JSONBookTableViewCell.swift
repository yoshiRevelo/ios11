//
//  JSONBookTableViewCell.swift
//  parserExample
//
//  Created by MobileStudio Laptop004 on 26/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class JSONBookTableViewCell: UITableViewCell {
    
    //MARK: - Outlets
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    var book:Book!{
        didSet{
            configureCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //MARK: - Private Methods
    private func configureCell(){
        titleLabel.text = book.title
        authorLabel.text = book.author
        priceLabel.text = book.price
    }
}
