//
//  ViewController.swift
//  keyboardExample
//
//  Created by MobileStudio Laptop005 on 21/04/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK: - Oulets
    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var lastNameTextField: UITextField!
    
    @IBOutlet weak var ageTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    //MARK: - Vars
    
    private var currentTextField: UITextField?
    
    //MARK: - overrideFunctions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //**** hay que asociar el delegate al viewController   *****
        // hay que implementar el UITextFieldDelegate
        //saber la ejecución de los metodos mediante delegate
        
        // self en este caso se refiere al viewController
        //asignar delegate por código
        nameTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Set Keyboard Notification
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    //ocultar teclado al tocar en cualquier parte de la pantalla
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //view.endEditing(true)
        currentTextField?.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - keyBoardFuctions
    
    @objc func keyBoardWillShow(notification: Notification){
        print("keyBoardWillShow")
        //Obtenemos el tamaño del teclado para modificar el constraint
        let userInfo = notification.userInfo!
        print(userInfo) // trae la info del teclado
        let keyBoardSize = userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let keyBoardFrame = keyBoardSize.cgRectValue
        
        bottomConstraint.constant = 10 + keyBoardFrame.height
        
        let animationDuration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! Double
        // Animamos la transición de los textField
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyBoardWillHide(notification: Notification){
        print("keyBoardWillHide")
        //Regresamos el formulario al la posición original
        bottomConstraint.constant = 10
        let userInfo = notification.userInfo!
        let animationDuration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! Double
        
        // Animamos la transición de los textField
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    
}

//MARK: - Extension

extension ViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameTextField:
            //metodo que se utiliza para saltar al otro textField
            lastNameTextField.becomeFirstResponder()
        case lastNameTextField:
            ageTextField.becomeFirstResponder()
        case emailTextField:
            passwordTextField.becomeFirstResponder()
        case passwordTextField:
            //deselecciona el textField y por lo tanto el teclado se oculta :D
            currentTextField?.resignFirstResponder()
        //self.view.endEditing(true)
        default:
            break
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        currentTextField = nil
    }
}
