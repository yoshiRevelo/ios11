//
//  ViewController.swift
//  calculadora
//
//  Created by MobileStudio Laptop005 on 14/04/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var displayLabel: UILabel!
    
    @IBOutlet weak var multiplyButton: UIButton!
    
    //MARK: - Constats
    let add = 1
    let sustract = 2
    let multiply = 3
    let divide = 4
    let result = 5
    
    //MARK: - Variables
    var currentOperation : Int?
    var operador : String?
    var currentValue : String?
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - User interaction
    //5524523072
    @IBAction func numberButtonPressed(_ sender: UIButton) {
        //print("You have clicked Button number: \(sender.titleLabel!.text!)")
        
        //El texto puede ser opcional
        if displayLabel.text == "0"{
            displayLabel.text = sender.titleLabel?.text
        }else{
            //asignación compuesta +=, -=, *=, /=
            displayLabel.text! += sender.titleLabel!.text!
            
            if currentOperation != nil{
                if let operador = operador{
                    self.operador = operador + sender.titleLabel!.text!
                }else{
                    operador = sender.titleLabel!.text!
                }
            }
            
            /*if currentOperation == nil{
                currentValue = displayLabel.text!
            }*/
            
            if currentOperation == nil{
                if let currentValue = currentValue{
                    self.currentValue = currentValue + sender.titleLabel!.text!
                }else{
                    currentValue = sender.titleLabel!.text!
                }
            }
        }
    }
    
    @IBAction func operationButtonPressed(_ sender: UIButton) {
        //print("You have pressed Button Operation: \(sender.tag)")
        
        switch sender.tag {
        case add:
            setupDisplayLabel(with: add, sender: sender)
        case sustract:
            setupDisplayLabel(with: sustract, sender: sender)
        case multiply:
            setupDisplayLabel(with: multiply, sender: sender)
            multiplyButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            multiplyButton.setTitleColor(#colorLiteral(red: 0.9527209052, green: 0.7188208587, blue: 0.4055068973, alpha: 1), for: .normal)
        case divide:
            setupDisplayLabel(with: divide, sender: sender)
        case result:
            print("result operation")
            if let operant1 = currentValue, let operant2 = operador{
                if let result = executeOperation(operant1, with: operant2) {
                    currentValue = "\(result)"
                    displayLabel.text = currentValue!
                    
                    //Reset
                    currentOperation = nil
                    operador = nil
                }
            }
        default:
            print("Opción no Válida ☠️")
        }
        
    }

    @IBAction func clearButtonPressed(_ sender: UIButton) {
        //print("You have pressed Clean Button")
        displayLabel.text = "0"
        currentOperation = nil
        operador = nil
        currentValue = nil
    }
    
    /*tag
    add = 1
    subs = 2
    mult = 3
    div = 4
    result = 5
     */
    
    func setupDisplayLabel(with operation : Int, sender : UIButton){
        if displayLabel.text != "0", currentOperation == nil{
            currentOperation = operation
            displayLabel.text! += sender.titleLabel!.text!
        }else if operador == nil{
            currentOperation = operation
            displayLabel.text?.removeLast()
            displayLabel.text! += sender.titleLabel!.text!
        }
    }
    
    func executeOperation(_ operant1 : String, with operant2 : String) -> Int? {
        //Switch inside
        guard let operation = currentOperation else {return nil}
        guard let value1 = Int(operant1), let value2 = Int(operant2) else {return nil}
        
        switch operation {
        case add:
            return value1 + value2
        case sustract:
            return value1 - value2
        case multiply:
            return value1 * value2
        case divide:
            return value1 / value2
        default:
            return nil
        }
    }
}

