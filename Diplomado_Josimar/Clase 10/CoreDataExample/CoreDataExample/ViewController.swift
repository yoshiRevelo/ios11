//
//  ViewController.swift
//  CoreDataExample
//
//  Created by MobileStudio Laptop004 on 16/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //insertExample()
        readExample()
        //updateExample()
        //deleteExample()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Private methods
    
    private func insertExample(){
        print("Insert...")
        
        let context = CoreDataManager.sharedInstance.persistentContainer.viewContext
        
        //Creamos la instancia Artist con el constructor de CoreData
        let artist = Artist(context: context)
        artist.name = "Juan Gabriel"
        
        //Creamos una instancia de Song
        let song = Song(context: context)
        song.title = "El noa noa"
        song.duration = 180
        
        //Se establece la relación entre la canción y el artista
        song.artist = artist
        
        let songTwo = Song(context: context)
        songTwo.title = "Te sigo amando"
        songTwo.duration = 240
        
        //Establecemos la relación
        artist.addToSongs(songTwo)
        
        //Guardamos los datos en la base
        CoreDataManager.sharedInstance.saveContext()
        print("Insert finished...")
    }
    
    private func readExample(){
        print("Read...")
        let artists = CoreDataManager.sharedInstance.readData(className: "Artist") as! [Artist]
        //let results: NSFetchRequest<NSFetchRequestResult> = Artist.fetchRequest()
        //let artists = CoreDataManager.sharedInstance.readDataV2(fetchRequest: results)
        
        print("Artistas encontrados: \(artists.count)")
        for artist in artists {
            print("Name: \(artist.name!)")
            
            for song in artist.songs?.allObjects as! [Song]{
                print("\t\(song.title!) - \(song.duration)s")
            }
        }
        
        let songs = CoreDataManager.sharedInstance.readData(className: "Song", key: "title", value: "El noa noa")
        print("Canciones encontradas: \(songs.count)")
        print("Generics")
        let genericArtists = CoreDataManager.sharedInstance.readDataWithGenerics(class: Artist.self)
        for a in genericArtists{
            print(a.name ?? "-")
        }
        
        print("Read finished...")
    }

    private func updateExample(){
        print("Update...")
        let artists = CoreDataManager.sharedInstance.readData(className: "Artist") as! [Artist]
        
        for artist in artists{
            artist.name = "\(artist.name!) UPDATED"
            
            for song in artist.songs?.allObjects as! [Song]{
                song.title = "\(song.title!) UPDATED"
            }
        }
       
        CoreDataManager.sharedInstance.saveContext()
        print("Update finished...")
    }
    
    private func deleteExample(){
        print("Delete...")
        let artists = CoreDataManager.sharedInstance.readData(className: "Artist") as! [Artist]
        
        let context = CoreDataManager.sharedInstance.persistentContainer.viewContext
        
        for artist in artists{
            context.delete(artist)
        }
        
        CoreDataManager.sharedInstance.saveContext()
        print("Delete finished...")
    }
}

