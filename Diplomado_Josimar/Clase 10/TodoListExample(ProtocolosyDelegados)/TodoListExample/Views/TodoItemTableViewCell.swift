//
//  TodoItemTableViewCell.swift
//  TodoListExample
//
//  Created by MobileStudio Laptop004 on 12/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class TodoItemTableViewCell: UITableViewCell {

    //MARK: - Outlets de la celda personalizada
    @IBOutlet weak var todoItemTitleLable: UILabel!
    @IBOutlet weak var colorIndicatorView: UIView!
    
    var todoItem : TodoItem!{
        didSet{
            configurecell()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - private methods
    
    private func configurecell(){
        todoItemTitleLable.text = todoItem.title
        if todoItem.done{
            //Crear attributes dict para la cadena de texto
            
            let attributesDict: [NSAttributedStringKey:Any] = [
                NSAttributedStringKey.strikethroughStyle : 1
            ]
            let attributedString = NSAttributedString(string: todoItem.title ?? "Sin título", attributes: attributesDict)
            todoItemTitleLable.attributedText = attributedString
        }else{
            todoItemTitleLable.text = todoItem.title
        }
        colorIndicatorView.backgroundColor = todoItem.color as? UIColor
    }
}
