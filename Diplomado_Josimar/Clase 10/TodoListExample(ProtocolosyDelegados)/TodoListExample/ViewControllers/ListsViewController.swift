//
//  ListsViewController.swift
//  TodoListExample
//
//  Created by MobileStudio Laptop004 on 16/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ListsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var dataSource : [List] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = CoreDataManager.sharedInstance.readData(class: List.self)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CreateListViewController" {
            let createListViewController = segue.destination as! CreateListViewController
            //4
            createListViewController.delegate = self
        }else if segue.identifier == "TodoListViewController"{
            let todoListViewController = segue.destination as! TodoListViewController
            todoListViewController.list = sender as! List
            
        }
        
    }
 

}

//MARK: - UITableViewDelegate, UITableViewDataSource
extension ListsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell")!
        cell.textLabel?.text = dataSource[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let list = dataSource[indexPath.row]
        performSegue(withIdentifier: "TodoListViewController", sender: list)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

//MARK: - UITableViewDelegate, UITableViewDataSource
extension ListsViewController: CreateListDelegate{
    func createListDidCreate(list: List) {
        dataSource.append(list)
        tableView.reloadData()
        navigationController?.popViewController(animated: true)
    }
}
