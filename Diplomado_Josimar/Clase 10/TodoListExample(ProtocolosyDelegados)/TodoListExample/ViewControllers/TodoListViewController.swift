//
//  TodoListViewController.swift
//  TodoListExample
//
//  Created by MobileStudio Laptop005 on 05/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class TodoListViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    var list: List!
    
    //MARK: - private vars
    private var dataSource : [TodoItem] = []
    private var isDetailActive = true
    
    //MARK: . override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        //dataSource = CoreDataManager.sharedInstance.readData(class: TodoItem.self)
        dataSource = list.items?.allObjects as! [TodoItem]
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        isDetailActive = UserDefaults.standard.bool(forKey: "DETAIL_ACTIVE")
        tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CreateTodoViewController" {
            let createTodoViewController = segue.destination as! CreateTodoViewController
            //4
            createTodoViewController.delegate = self
        }
    }
 

    
    //MARK: - private methods
    private func createDummyData(){
        let todoDict : [String : Any] = [
            "title": "Estudiar Swift",
            "notes": "Buscar ejemplos de swift y entenderlos",
            "done" : false
        ]
        
        let todoDict2 : [String : Any] = [
            "title": "Comprar leche",
            "notes": "Deslactosada porque me hace daño la entera",
            "done" : false
        ]
        
        let todoDict3 : [String : Any] = [
            "title": "Lavar ropa",
            "notes": "Ya no tengo ropa limpia",
            "done" : true
        ]
        
        let todos = [
            "todos" : [todoDict, todoDict2, todoDict3]
        ]
        
        writeDictToJson(dict: todos, fileName: "todos.json")
    }
    
    private func writeDictToJson(dict : [String :  Any], fileName: String){
        //hay que serializarlo para obtener data
        // se serializa diccionarios o arreglos <-> json
        let data = try! JSONSerialization.data(withJSONObject: dict, options: [.prettyPrinted])
        
        //El el archivo json lo debemos codificar en utf8
        let jsonString = String(data: data , encoding: .utf8)
        
        //ruta del Documents donde vamos a escribir el archivo
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        
        //ruta y nombre que le vamos a poner al archivo
        let completePaths = paths[0].appending("/\(fileName)")
        print(completePaths)
        
        // do try - catch
        do{
            try jsonString?.write(toFile: completePaths, atomically: true, encoding: .utf8)
        }catch let error{
            print("Save file error: \(error.localizedDescription)")
        }
    }
    
    private func readJsonToDict(fileName: String)-> [String:Any]?{
        
        //Obtenemos el directorio documentos
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        
        //Creamos la ruta completa del archivo
        let completePath = paths[0].appending("/\(fileName)")
        
        //Creamosla variable en donde vamos a vaciar el contenido del archivo
        var dict: [String:Any]?
        
        do{
            let jsonString = try String(contentsOfFile: completePath, encoding: .utf8)
            //print("JSON String: \(jsonString)")
            
            //Transformamos nuestro string a Data y posteriormente a un JSON Object
            if let data = jsonString.data(using: .utf8){
                dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
            }
            
        }catch let error{
            print("Reading Error: \(error.localizedDescription)")
        }
        return dict
    }
}

//MARK: - UITableViewDataSource, UITableViewDelegate
extension TodoListViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isDetailActive{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TodoItemDetailTableViewCell") as! TodoItemDetailTableViewCell
            cell.todoItem = dataSource[indexPath.row]
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TodoItemTableViewCell") as! TodoItemTableViewCell
            cell.todoItem = dataSource[indexPath.row]
            return cell
        }
        
        
        // con MVC
        /*let todoItem = dataSource[indexPath.row]
        cell.todoItemTitleLable.text = todoItem.title
        if todoItem.done{
            cell.colorIndicatorView.backgroundColor = .green
        }else{
            cell.colorIndicatorView.backgroundColor = .red
        }*/
        
        // con MVVM (Model View ViewModel)
        
        
        //cell.textLabel?.text = "Hola"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

//MARK: - Protoolo CreateTodoDelegate
//3
extension TodoListViewController: CreateTodoDelegate{
    func createTodoDidCreate(todoItem: TodoItem) {
        //Aquí recibimos el nuevo todo desde el viewController de creación
        todoItem.list = list
        CoreDataManager.sharedInstance.saveContext()
        dataSource.append(todoItem)
        tableView.reloadData()
        navigationController?.popViewController(animated: true)
    }
}
