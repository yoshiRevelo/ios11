//
//  CreateTodoViewController.swift
//  TodoListExample
//
//  Created by MobileStudio Laptop004 on 19/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit


//MARK: - Procotol & Delegate

//heredar de class para evitar retainCycle
//1
protocol CreateTodoDelegate: class {
    func createTodoDidCreate(todoItem: TodoItem)
}

class CreateTodoViewController: UIViewController {
   //2
    weak var delegate: CreateTodoDelegate?
    
    //MARK: - Private vars
    private var selectedColor = UIColor.lightGray
    
    //MARK: - Outlets
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var notesTextField: UITextField!
    @IBOutlet weak var ButtonColor: UIButton!
    @IBOutlet weak var createButtonBottomConstraint: NSLayoutConstraint!
    
    //MARK: - systemMethods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWollShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ButtonColorViewController"{
            let buttonColorViewController = segue.destination as! ButtonColorViewController
            
            buttonColorViewController.delegate = self
            view.endEditing(true)
        }
    }

    //MARK: - Private Methods
    @objc private func keyboardWollShow(notification: Notification){
        let userInfo = notification.userInfo!
        let keyboardSize = userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let keyboardFrame = keyboardSize.cgRectValue
        
        createButtonBottomConstraint.constant = keyboardFrame.height
        UIView.animate(withDuration: 0.25){
            self.view.layoutIfNeeded()
        }
        
    }
    
    @objc private func keyBoardWillHide(notification: Notification){
        createButtonBottomConstraint.constant = 0
        self.view.layoutIfNeeded()
    }
    
    //MARK: - User Interaction
    
    @IBAction func createTodoItemButtonPressed(_ sender: Any) {
        guard let title = titleTextField.text, !title.isEmpty else { return }
        guard let notes = notesTextField.text else { return }
        
        //let todoItem = TodoItem(title: title, notes: notes, done: false, color: selectedColor)
        let context = CoreDataManager.sharedInstance.persistentContainer.viewContext
        let todoItem = TodoItem(context: context)
        
        todoItem.title = title
        todoItem.notes = notes
        todoItem.color = selectedColor
        todoItem.done = false
        print("TodoItem: \(todoItem.title!)")
        CoreDataManager.sharedInstance.saveContext()
        //5
        delegate?.createTodoDidCreate(todoItem: todoItem)
    }
}

//MARK: - SelectColorPressedDelegate
extension CreateTodoViewController: ChangeButtonColor{
    func selectColorPressed(sender: UIButton) {
        selectedColor = sender.backgroundColor!
        ButtonColor.backgroundColor = sender.backgroundColor
        //Volver a la pantalla anterior
        navigationController?.popViewController(animated: true)
    }
}


