//
//  CreateListViewController.swift
//  TodoListExample
//
//  Created by MobileStudio Laptop004 on 16/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

protocol CreateListDelegate: class {
    func createListDidCreate(list: List)
}

class CreateListViewController: UIViewController {

    weak var delegate: CreateListDelegate?
    
    @IBOutlet weak var titleTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - User Interaction
    @IBAction func createButtonPressed(_ sender: Any) {
        guard let title = titleTextField.text, !title.isEmpty else { return }
        let context = CoreDataManager.sharedInstance.persistentContainer.viewContext
        let list = List(context: context)
        list.name = title
        CoreDataManager.sharedInstance.saveContext()
        //5
        print(list)
        delegate?.createListDidCreate(list: list)
    }
    
}
