//
//  TodoItem+CoreDataProperties.swift
//  TodoListExample
//
//  Created by MobileStudio Laptop004 on 16/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//
//

import Foundation
import CoreData


extension TodoItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TodoItem> {
        return NSFetchRequest<TodoItem>(entityName: "TodoItem")
    }

    @NSManaged public var title: String?
    @NSManaged public var notes: String?
    @NSManaged public var done: Bool
    @NSManaged public var color: NSObject?
    @NSManaged public var list: List?

}
