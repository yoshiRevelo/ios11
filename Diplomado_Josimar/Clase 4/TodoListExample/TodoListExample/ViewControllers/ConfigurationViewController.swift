//
//  ConfigurationViewController.swift
//  TodoListExample
//
//  Created by MobileStudio Laptop004 on 12/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ConfigurationViewController: UIViewController {

    //MARK: - Outlets
    
    @IBOutlet weak var detailSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let isDetailActive = UserDefaults.standard.bool(forKey: "DETAIL_ACTIVE")
        detailSwitch.setOn(isDetailActive, animated: false)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - User Interaction
    
    @IBAction func detailSwitchChanged(_ sender: Any) {
        print("cambia")
        
        UserDefaults.standard.set(detailSwitch.isOn, forKey: "DETAIL_ACTIVE")
        UserDefaults.standard.synchronize()
    }
    
}
