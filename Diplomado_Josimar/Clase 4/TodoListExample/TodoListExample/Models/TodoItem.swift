//
//  TodoItem.swift
//  TodoListExample
//
//  Created by MobileStudio Laptop005 on 05/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation
class TodoItem {
    var title = ""
    var notes: String?
    var done = false
    
    init(title: String, notes: String?, done:Bool) {
        self.title = title
        self.notes = notes
        self.done = done
    }
}
