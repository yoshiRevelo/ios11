//
//  TodoItemDetailTableViewCell.swift
//  TodoListExample
//
//  Created by MobileStudio Laptop004 on 12/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class TodoItemDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var cellTitleLabel: UILabel!  
    @IBOutlet weak var cellnotesLabel: UILabel!
    @IBOutlet weak var colorIndicatorView: UIView!
    
    var todoItem : TodoItem!{
        // Key Value Observer -> Property observer in Swift
        didSet{
            configurecell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

        //MARK: - private methods
    private func configurecell(){
        cellTitleLabel.text = todoItem.title
        cellnotesLabel.text = todoItem.notes
        
        if todoItem.done{
            colorIndicatorView.backgroundColor = .green
            let attributesDict: [NSAttributedStringKey:Any] = [
                NSAttributedStringKey.strikethroughStyle : 1
            ]
            
            let attributedString = NSAttributedString(string: todoItem.title, attributes: attributesDict)
            
            cellTitleLabel.attributedText = attributedString
        }else{
            colorIndicatorView.backgroundColor = .red
            cellTitleLabel.text = todoItem.title
        }
    }
}
