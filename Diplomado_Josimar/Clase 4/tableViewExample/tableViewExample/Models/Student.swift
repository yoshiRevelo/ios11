//
//  Student.swift
//  tableViewExample
//
//  Created by MobileStudio Laptop005 on 05/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

class Student{
    var name : String
    var programmingLanguage : String
    
    init(name: String, programmingLanguage: String) {
        self.name = name
        self.programmingLanguage = programmingLanguage
    }
}
