//
//  StudentDetailViewController.swift
//  tableViewExample
//
//  Created by MobileStudio Laptop005 on 05/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class StudentDetailViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var programmingLanguageLabel: UILabel!
    
    var student : Student!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = student.name
        programmingLanguageLabel.text = student.programmingLanguage
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
