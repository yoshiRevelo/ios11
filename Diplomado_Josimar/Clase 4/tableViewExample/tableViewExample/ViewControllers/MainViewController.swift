//
//  MainViewController.swift
//  tableViewExample
//
//  Created by MobileStudio Laptop005 on 05/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    //MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var editButton: UIBarButtonItem!
    
    //MARK: - private vars
    private var planetsArray: [Planet] = []
    private var studentsArray : [Student] = []
    
    //MARK - overrideFuncs
    override func viewDidLoad() {
        super.viewDidLoad()
        
        planetsArray.append(Planet(name: "Mercurio", planetDescription: "Descripción de Mercurio"))
        planetsArray.append(Planet(name: "Venus", planetDescription: "Descripción de Venus"))
        planetsArray.append(Planet(name: "Tierra", planetDescription: "Descripción de Tierra"))
        planetsArray.append(Planet(name: "Marte", planetDescription: "Descripción de Marte"))
        planetsArray.append(Planet(name: "Júpiter", planetDescription: "Descripción de Júpiter"))
        planetsArray.append(Planet(name: "Saturno", planetDescription: "Descripción de Saturno"))
        planetsArray.append(Planet(name: "Urano", planetDescription: "Descripción de Urano"))
        planetsArray.append(Planet(name: "Neptuno", planetDescription: "Descripción de Neptuno"))
        planetsArray.append(Planet(name: "Plutón", planetDescription: "Descripción de Plutón"))
        
        //Estudiantes
        
        studentsArray.append(Student(name: "Ricardo López", programmingLanguage: "Swift 2.0"))
        studentsArray.append(Student(name: "Josimar Revelo", programmingLanguage: "Swift 3.1"))
        studentsArray.append(Student(name: "Edgar Santiago", programmingLanguage: "Swift 3.2"))
        studentsArray.append(Student(name: "César Guzmán", programmingLanguage: "Swift 4.1"))
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PlanetDetailViewController"{
            let planetDetailViewController = segue.destination as! PlanetDetailViewController
            planetDetailViewController.planet = sender as! Planet
        } else if segue.identifier == "StudentDetailViewController"{
            let studentDetailViewController = segue.destination as! StudentDetailViewController
            studentDetailViewController.student = sender as! Student
        }
    }
    
    //este es un navButton para el Navigation
    @IBAction func editButtonPressed(_ sender: Any) {
        tableView.setEditing(!tableView.isEditing, animated: true)
        editButton.title = tableView.isEditing ? "Listo" : "Editar"
    }
    
    
}

extension MainViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    //este mètodo nos indica cuantas celdas se van a mostrar en la sección dada
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return planetsArray.count
        }else{
            return studentsArray.count
        }
    }
    
    //este método nos crea la UI de la celda que se va a mostrar en el indexPath dado
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // poner el ! porque sabemos que existe
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "planetTableViewCell")!
            let planet = planetsArray[indexPath.row]
            cell.textLabel?.text = planet.name
            cell.accessoryType = .disclosureIndicator
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "StudentTableViewCell")!
            let student = studentsArray[indexPath.row]
            cell.textLabel?.text = student.name
            cell.detailTextLabel?.text = student.programmingLanguage
            cell.accessoryType = .checkmark
            return cell
        }
    }

    //Título de la sección
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Planetas"
        }else{
            return "Estudiantes"
        }
    }
    
    // este método nos comunica cuando el usuario da clic
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            let planet = planetsArray[indexPath.row]
            print(indexPath)
            performSegue(withIdentifier: "PlanetDetailViewController", sender: planet)
            tableView.deselectRow(at: indexPath, animated: true)
        }else{
            tableView.deselectRow(at: indexPath, animated: true)
            let student = studentsArray[indexPath.row]
            print(indexPath)
            performSegue(withIdentifier: "StudentDetailViewController", sender: student)
        }
    }
    
    //Este método nos dice que celdas se pueden editar y cuales no
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 0{
            return false
        }else{
            return true
        }
    }
    
    //Este método se llama cuando el usuario realiza una acción de edición
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        print("Aquí ☺️")
        if editingStyle == .delete {
            //Eliminamos el estudiante del arreglo y la celda de la tabla
            //siempre deben de coincidir ambos
            studentsArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    //para cambiar el texto del botón de eliminar
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "😭"
    }
    
    //Nos dice si se puede mover la celda dependiendo del index path
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    //Nos dice de donde a donde se quiere mover la celda
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        print(sourceIndexPath)
        let studentToMove = studentsArray[sourceIndexPath.row]
        studentsArray.remove(at: sourceIndexPath.row)
        studentsArray.insert(studentToMove, at: destinationIndexPath.row)
    }
    
    func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        if sourceIndexPath.section == proposedDestinationIndexPath.section{
            return proposedDestinationIndexPath
        }else{
            return sourceIndexPath
        }
    }
}
