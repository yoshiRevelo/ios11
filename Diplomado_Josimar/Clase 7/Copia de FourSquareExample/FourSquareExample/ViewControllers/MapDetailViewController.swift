//
//  MapDetailViewController.swift
//  FourSquareExample
//
//  Created by MobileStudio Laptop004 on 02/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import MapKit

class MapDetailViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var longitudLabel: UILabel!
    
    
    var mapRoute: MKAnnotation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.addAnnotation(mapRoute)
        configureMap()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK: - private methods
    private func configureMap(){
        print("esto trae: \(mapRoute.coordinate.)")
        latitudeLabel.text = "\(mapRoute.coordinate.latitude)"
        longitudLabel.text = "\(mapRoute.coordinate.longitude)"
    }
}
