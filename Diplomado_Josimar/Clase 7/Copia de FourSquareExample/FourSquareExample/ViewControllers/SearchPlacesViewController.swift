//
//  SearchPlacesViewController.swift
//  FourSquareExample
//
//  Created by MobileStudio Laptop004 on 26/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import Alamofire
import  MapKit

class SearchPlacesViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var radiusSlider: UISlider!
    
    private var locationManager = CLLocationManager()
    
    private var userLatitude: Double!
    private var userLongitude: Double!
    private var userQuery = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //searchPlaces(query: "pizza")
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        
        //locationManager.requestWhenInUseAuthorization()
        //locationManager.requestLocation()
        
        /*
        let annotation = MKPointAnnotation()
        let coordinate = CLLocationCoordinate2D(latitude: userLatitude, longitude: userLongitude)
        annotation.coordinate = coordinate
        annotation.title = "MobileStudio"
        
        mapView.addAnnotation(annotation)
 */
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        _ = checkPermissions()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MapDetailViewController"{
            let mapDetailViewController = segue.destination as! MapDetailViewController
            mapDetailViewController.mapRoute = sender as? MKAnnotation
        }
    }
    
    //MARK: - Private Methods
    
    private func checkPermissions() -> Bool{
        if CLLocationManager.locationServicesEnabled(){
            let status = CLLocationManager.authorizationStatus()
            if status == .authorizedWhenInUse{
                print("podemos usar el GPS")
                return true
            }else if status == .notDetermined{
                locationManager.requestWhenInUseAuthorization()
                return true
            }else if status == .restricted || status == .denied{
                //TODO: Pedir al usuario que active su GPS
                print("no hay permiso :(")
                locationServicesAlert()
                return false
            }
            else{
                locationServicesAlert()
                return false
            }
        }
        else {
            return false
        }
    }
    
    private func locationServicesAlert(){
        let alertController = UIAlertController(title: "Oups!", message: "Para poder usar esta aplicación, necesitamos saber tu ubicación.", preferredStyle: .alert)
        
        let configAction = UIAlertAction(title: "Ir a configuración", style: .default) { (action) in
            if let url = URL(string: UIApplicationOpenSettingsURLString){
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(configAction)
        let cancelAction = UIAlertAction(title: "Cancelar", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    private func searchPlaces(query: String){
        guard  let latitude = userLatitude else {
            print("latitude en nil")
            return
            
        }
        guard let longitude = userLongitude else {
            print("longitud en nil")
            return
            
        }
        searchTextField.resignFirstResponder()
        
        let params = [
            "client_id": Constants.FourSquareAPI.clientId,
            "client_secret": Constants.FourSquareAPI.secretId,
            "v": "20180526",
            "ll": "\(latitude), \(longitude)",
            "radius": "\(radiusSlider.value)",
            "query": query
        ]
        
        Alamofire.request(Constants.FourSquareAPI.baseURL, parameters: params)
        .validate()
            .responseData { (response) in
                
                //print("RequetURL: \(response.request?.url?.absoluteURL)")
                switch response.result {
                case .success(let value):
                    print("Data: \(value)")
                    do{
                        let foursquareResponse = try JSONDecoder().decode(FourSquareResposeModel.self, from: value)
                        self.showPlacesOnMap(placesArray: foursquareResponse.response.venues)
                        print("Lugares encontrados: \(foursquareResponse.response.venues.count)")
                    }catch(let error){
                        print("Codable error: \(error.localizedDescription)")
                    }
                case .failure(let error):
                        print ("Request error: \(error.localizedDescription)")
                }
        }
    }
    
    private func showPlacesOnMap(placesArray: [VenueModel]){
        clearRoutes()
        clearPlaces()
        
        for venue in placesArray{
            let annotation = MKPointAnnotation()
            let coordinate = CLLocationCoordinate2D(latitude: venue.location.latitude, longitude: venue.location.longitude)
            annotation.coordinate = coordinate
            annotation.title = venue.name
            mapView.addAnnotation(annotation)
        }
        
        let userCenter = CLLocationCoordinate2D(latitude: userLatitude, longitude: userLongitude)
        let region = MKCoordinateRegionMakeWithDistance(userCenter, 1000, 1000)
        mapView.setRegion(region, animated: true)
    }
    
    private func clearPlaces(){
        let currentAnnotations = mapView.annotations
        mapView.removeAnnotations(currentAnnotations)
    }
    
    private func clearRoutes(){
        let currentOverlays = mapView.overlays
        mapView.removeOverlays(currentOverlays)
    }
    
    private func loadRouteFor(annotation: MKAnnotation){
        
        clearRoutes()
        
        //MapItem a través de un placeMark a través de coordenadas
        let placeMark = MKPlacemark(coordinate: annotation.coordinate)
        let destinationMapItem = MKMapItem(placemark: placeMark)
        
        let request = MKDirectionsRequest()
        request.transportType = .walking
        request.requestsAlternateRoutes = false
        request.source = MKMapItem.forCurrentLocation()
        request.destination = destinationMapItem
        
        let directions = MKDirections(request: request)
        
        directions.calculate { (response, error) in
            if let error = error{
                print("Directions error: \(error.localizedDescription)")
            }
            else{
                if let routes = response?.routes{
                    for route in routes{
                        for step in route.steps{
                            print(step.instructions)
                        }
                        self.mapView.add(route.polyline, level: .aboveRoads)
                    }
                }
            }
        }
    }
}


//MARK: - UITextFieldDelegate
//hay que conectar el delegate
extension SearchPlacesViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if checkPermissions(){
            if let searchQuery = searchTextField.text{
                userQuery = searchQuery
                print("El usuario busca \(searchQuery)")
                locationManager.requestLocation()
            }
        }
        //print("El usuario presionó enter")
        return true
    }
}

//MARK: - CLLocationManagerDelegate
extension SearchPlacesViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //print("localización encontrada")
        if let location = locations.first{
            print("USER GPS Latitude: \(location.coordinate.latitude)")
            print("USER GPS Longitude: \(location.coordinate.longitude)")
            userLatitude = location.coordinate.latitude
            userLongitude = location.coordinate.longitude
            searchPlaces(query: userQuery)
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error location: \(error.localizedDescription)")
    }
}

//MARK: - MKMapViewDelegate
extension SearchPlacesViewController: MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation{
            return nil
        }
        
        let annotationIdentifier = "VenueAnnotationIdentifier"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            
            //Mostrar info de el lugar
            annotationView?.canShowCallout = true
        }else{
            annotationView?.annotation = annotation
        }
        
        annotationView?.image = #imageLiteral(resourceName: "pin_blue2")
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let annotation = view.annotation{
            loadRouteFor(annotation: annotation)
            performSegue(withIdentifier: "MapDetailViewController", sender: annotation)
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = .red
        renderer.lineWidth = 10
        renderer.alpha = 0.5
        return renderer
    }
}
