//
//  VenueModel.swift
//  FourSquareExample
//
//  Created by MobileStudio Laptop004 on 26/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

class VenueModel: Codable {
    var name : String
    var location : LocationModel
}
