//
//  LocationModel.swift
//  FourSquareExample
//
//  Created by MobileStudio Laptop004 on 26/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

//jsonviewer
//jsonformat

class LocationModel: Codable {
    var latitude: Double
    var longitude: Double
    
    enum CodingKeys: String, CodingKey{
        case latitude = "lat"
        case longitude = "lng"
    }
}
