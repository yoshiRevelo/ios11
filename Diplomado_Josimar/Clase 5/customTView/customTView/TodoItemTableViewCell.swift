//
//  TodoItemTableViewCell.swift
//  customTView
//
//  Created by MobileStudio Laptop004 on 12/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class TodoItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var todoItemTitleLabel: UILabel!
    @IBOutlet weak var todoItemSubtitleLabel: UILabel!
    @IBOutlet weak var todoItemDegreesLabel: UILabel!
    @IBOutlet weak var todoItemBackgroundImage: UIImageView!
    @IBOutlet weak var todoitemWeatherImage: UIImageView!
    
    var todoItem : TodoItem!{
        didSet{
            configurecell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func configurecell(){
        todoItemBackgroundImage.image = todoItem.backgorund
        todoItemTitleLabel.text = todoItem.pais
        todoItemSubtitleLabel.text = todoItem.localidad
        todoItemDegreesLabel.text = todoItem.grados
        todoitemWeatherImage.image = todoItem.clima
    }

}
