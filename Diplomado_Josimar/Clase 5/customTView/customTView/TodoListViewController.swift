//
//  TodoListViewController.swift
//  customTView
//
//  Created by MobileStudio Laptop004 on 12/05/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class TodoListViewController: UIViewController {
    
    var datos : [TodoItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        datos.append(TodoItem(backgorund: #imageLiteral(resourceName: "mexico"), pais: "México", localidad: "CDMX", grados: "8", clima: #imageLiteral(resourceName: "cloudy")))
        datos.append(TodoItem(backgorund: #imageLiteral(resourceName: "paris"), pais: "Francia", localidad: "Paris", grados: "24", clima: #imageLiteral(resourceName: "sunny")))
        datos.append(TodoItem(backgorund: #imageLiteral(resourceName: "newyork"), pais: "USA", localidad: "New York", grados: "12", clima: #imageLiteral(resourceName: "windy")))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

//MARK: - UITableViewDataSource, UITableViewDelegate

extension TodoListViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "TodoItemTableViewCell") as! TodoItemTableViewCell
        cell.todoItem = datos[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
}
