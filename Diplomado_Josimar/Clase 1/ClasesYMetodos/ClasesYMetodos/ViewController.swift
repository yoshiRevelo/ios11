//
//  ViewController.swift
//  ClasesYMetodos
//
//  Created by MobileStudio Laptop005 on 14/04/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Hola Mundo")
        
        let x = 0 // constante implicita tipo int
        let name = "Josi" // constante implicit tipo string
        
        let y: Int = 1 // Constante explicita tipo int
        let lastname: String = "Revelo" // constante explìcita tipo String
        
        print(x)
        print("\(x) - \(name) - \(y) - \(lastname)")
        
        var x2 = 0
        var y2: Int = 2
        
        //////////////////// Arreglos /////////////////
        
        print("--------arreglos--------")
        let myArray = [1,2,3,4,5]
        let myArray2 = [1,2,3]
        let myArray3 : Array<Int> = Array()
        let myArray4 : [Int] = []
        let myArray5 : [Any] = [1,3.1416,"Hola"]
        
        print(myArray)
        print(myArray5)
        
        for number in myArray{
            print("Numero: \(number)")
        }
        for x in 0..<myArray.count{
            print("Numero tradicional: \(myArray[x])")
        }
        
        print("--------diccionarios--------")
        let myDictionary = [
            "key" : "value",
            "name" : "Josi",
            "lastname" : "Revelo"
        ]
        
        print(myDictionary)
        print(myDictionary["name"]!)
        
        var myDictionary2 : [String:Int] = [:]
        myDictionary2["uno"] = 1
        myDictionary2["dos"] = 2
        myDictionary2["tres"] = 3
        
        print(myDictionary2["dos"])
        
        if let dos = myDictionary2["dos"] {
            print("Valor  dos: \(dos)")
        }else{
            print("dos no tiene valor")
        }
        
        let stringOptional : String? = nil
        let stringOptional2 : String? = "Testing Optionals"
        
        print(stringOptional)
        print(stringOptional2)
        
        let keysArray = Array(myDictionary.keys)
        let valuesArray = Array(myDictionary.values)
        
        for key in keysArray{
            print("key: \(key) -> value: \(myDictionary[key] ?? "No value")")
        }
        
        print("--------funciones--------")
        sayHello()
        
        let result = suma(numA: 4, numB: 5)
        print("result: \(result)")
        
        let result1 = sumaSinLabel(5, 4)
        let result2 = sumaConLabelDiferente(x: 5, y: 4)
        
        print("result1: \(result1) - result2: \(result2)" )
        
        let resultResta = resta(numA: 5, numB: 3)
        let resultResta2 = resta(numA: 3, numB: 2, numC: 1)
        
        print(resultResta)
        print(resultResta2)
        
        print(multiplesValores().1)
        let josi = multiplesValores2()
        print("Nombre: \(josi.age)")
        
        print("--------clases--------")
        
        let person = Person(name: "Josi Revelo", age: 25)
        print(person.name)
        print(person.age)
        
        let developer = Developer(name: "Josi Revelo", age: 35, programmingLanguage: "Swift")
        print(developer.programmingLanguage)
        
        print(developer.saludo())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - Private Methods
    
    private func sayHello(){
        print("Hello Func")
    }
    
    private func suma(numA : Int, numB : Int) -> Int{
        return numA + numB
    }
    
    private func sumaSinLabel(_ numA:Int, _ numB:Int) -> Int{
        return numA + numB
    }
    
    private func sumaConLabelDiferente(x numA:Int, y numB:Int)->Int{
        return numA + numB
    }
    
    private func resta(numA:Int, numB:Int, numC:Int? = nil) -> Int{
        if let numC = numC{
            return numA-numB-numC
        }else{
            return numA-numB
        }
    }
    
    private func multiplesValores() -> (Int,String,Double){
        return (25, "Josi", 3.5)
    }
    
    private func multiplesValores2() -> (age: Int, name: String, height: Double){
        return (25, "Josi", 1.68)
    }
}

