//
//  Person.swift
//  ClasesYMetodos
//
//  Created by MobileStudio Laptop005 on 14/04/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

class Person{
    
    var name = ""
    var age = 0
    
    init(name:String, age:Int){
        self.name = name
        self.age = age
    }
    
    func saludo(){
        print("Hola Soy: \(name),  mi edad es: \(age)")
    }
}
