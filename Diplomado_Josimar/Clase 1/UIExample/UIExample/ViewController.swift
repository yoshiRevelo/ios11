//
//  ViewController.swift
//  UIExample
//
//  Created by MobileStudio Laptop005 on 14/04/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var helloWordLabel: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        helloWordLabel.text = "Hola Mundo Cruel"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - User interaction
    
    @IBAction func leftButtonPressed(_ sender: Any) {
        helloWordLabel.textAlignment = .left
    }
    
    @IBAction func centerButtonPressed(_ sender: Any) {
        helloWordLabel.textAlignment = .center
    }
    
    @IBAction func rightButtonPressed(_ sender: Any) {
        helloWordLabel.textAlignment = .right
    }
    
    @IBAction func redButtonPressed(_ sender: Any) {
        helloWordLabel.textColor = .red
    }
    
    @IBAction func blackButtonPressed(_ sender: Any) {
        helloWordLabel.textColor = .black
    }
    
    @IBAction func blueButtonPressed(_ sender: Any) {
        helloWordLabel.textColor = .blue
    }
    
    @IBAction func sliderChanged(_ sender: UISlider) {
        imageView.alpha = CGFloat(sender.value)
    }
    
    
}

