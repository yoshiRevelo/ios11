//
//  SelectedCourseVocabulary.swift
//  ExamVerbs
//
//  Created by Josimar Revelo on 14/07/18.
//  Copyright © 2018 Josimar Revelo. All rights reserved.
//

import Foundation

class SelectedCourseVocabulary: Codable {
    var description = ""
    var image = ""
    var verb = ""
    var courseId = ""
    
    enum CodingKeys: String, CodingKey{
        case description = "description"
        case image = "image"
        case verb = "verb"
    }
}
