//
//  CourseLevelModel.swift
//  ExamVerbs
//
//  Created by Josimar Revelo on 14/07/18.
//  Copyright © 2018 Josimar Revelo. All rights reserved.
//

import Foundation

class CourseLevelModel: Codable {
    var courseId = ""
    var description = ""
    var name = ""
    
    enum CodingKeys: String,CodingKey {
        case description = "description"
        case name = "name"
    }
}
