//
//  EnglishLabelWDTableViewCell.swift
//  ExamVerbs
//
//  Created by MobileStudioiMac02 on 28/07/18.
//  Copyright © 2018 Josimar Revelo. All rights reserved.
//

import UIKit

class EnglishLabelWDTableViewCell: UITableViewCell {

    var course: CourseLevelModel!{
        didSet{
            configureCell()
        }
    }
    
    @IBOutlet var CourseNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    private func configureCell(){
        CourseNameLabel.text = course.name
    }
    
}
