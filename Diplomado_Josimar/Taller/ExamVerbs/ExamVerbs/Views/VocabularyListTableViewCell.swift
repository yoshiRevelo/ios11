//
//  VocabularyListTableViewCell.swift
//  ExamVerbs
//
//  Created by Josimar Revelo on 14/07/18.
//  Copyright © 2018 Josimar Revelo. All rights reserved.
//

import UIKit

class VocabularyListTableViewCell: UITableViewCell {
    //MARK: - Outlets
    @IBOutlet weak var verbLabel: UILabel!
    @IBOutlet var wordImage: UIImageView!
    
    var selectedWord: SelectedCourseVocabulary!{
        didSet{
            configureCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //MARK: - private methods
    private func configureCell(){
        verbLabel.text = selectedWord.verb
        
        if let url = URL(string: selectedWord.image) {
            if let data = try? Data(contentsOf: url){
                wordImage.image = UIImage(data: data)
            }
        }
    }
}
