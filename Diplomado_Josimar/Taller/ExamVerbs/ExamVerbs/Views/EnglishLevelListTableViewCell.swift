//
//  EnglishLevelListTableViewCell.swift
//  ExamVerbs
//
//  Created by Josimar Revelo on 14/07/18.
//  Copyright © 2018 Josimar Revelo. All rights reserved.
//

import UIKit

class EnglishLevelListTableViewCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var CourseNameLabel: UILabel!
    @IBOutlet weak var CourseDescriptionLabel: UILabel!
    
    
    var course: CourseLevelModel!{
        didSet{
            configureCell()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //MARK: - Private Methods
    
    private func configureCell(){
        CourseNameLabel.text = course.name
        CourseDescriptionLabel.text = course.description
    }
}
