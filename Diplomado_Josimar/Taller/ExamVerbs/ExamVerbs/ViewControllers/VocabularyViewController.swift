//
//  VocabularyViewController.swift
//  ExamVerbs
//
//  Created by Josimar Revelo on 14/07/18.
//  Copyright © 2018 Josimar Revelo. All rights reserved.
//

import UIKit
import FirebaseDatabase

class VocabularyViewController: UIViewController {

    private var dataSource: [SelectedCourseVocabulary] = []
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    var englishLevelSelected: CourseLevelModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        //print(englishLevelSelected.courseId)
        loadVocabularyCourse()
        self.title = "\(englishLevelSelected.name) vocabulary"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SelectedWordViewController"{
            let selectedWordViewController = segue.destination as! SelectedWordViewController
            selectedWordViewController.selectedWord = sender as! SelectedCourseVocabulary
        }
    }
    
    //MARK: - private methods
    
    private func loadVocabularyCourse(){
        let vocabularyReference = Database.database().reference().child("vocabulary").child(englishLevelSelected.courseId)
        vocabularyReference.observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists(){
                if let jsonObject = snapshot.value as? [String:Any]{
                    for (key,value) in jsonObject{
                        //print(key)
                        //print(value)
                        let jsonData = try! JSONSerialization.data(withJSONObject: value, options: [])
                        let selectedCourseVocabulary = try! JSONDecoder().decode(SelectedCourseVocabulary.self, from: jsonData)
                        selectedCourseVocabulary.courseId = key
                        self.dataSource.append(selectedCourseVocabulary)
                    }
                    //print(self.dataSource)
                    self.dataSource = self.dataSource.sorted(by: { $0.verb < $1.verb })
                    self.tableView.reloadData()
                }
            }
        }
    }

}

extension VocabularyViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VocabularyListTableViewCell") as! VocabularyListTableViewCell
        cell.selectedWord = dataSource[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let wordSelected = dataSource[indexPath.row]
        performSegue(withIdentifier: "SelectedWordViewController", sender: wordSelected)
    }
    
    
}
