//
//  SelectedWordViewController.swift
//  ExamVerbs
//
//  Created by Josimar Revelo on 14/07/18.
//  Copyright © 2018 Josimar Revelo. All rights reserved.
//

import UIKit
import AVFoundation

class SelectedWordViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var verbLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var wordImageView: UIImageView!
    
    
    var selectedWord: SelectedCourseVocabulary!
    let synth = AVSpeechSynthesizer()
    var myUtterance = AVSpeechUtterance(string: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViewController()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - User Interaction
    @IBAction func speechWordButtonPressed(_ sender: Any) {
        myUtterance = AVSpeechUtterance(string: verbLabel.text!)
        myUtterance.rate = 0.5
        synth.speak(myUtterance)
    }
    
    //MARK: - private methods
    private func setViewController(){
        verbLabel.text = selectedWord.verb
        descriptionLabel.text = selectedWord.description
        
        if let url = URL(string: selectedWord.image) {
            if let data = try? Data(contentsOf: url){
                wordImageView.image = UIImage(data: data)
            }
        }
    }

}
