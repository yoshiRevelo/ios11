//
//  EnglishLevelViewController.swift
//  ExamVerbs
//
//  Created by Josimar Revelo on 14/07/18.
//  Copyright © 2018 Josimar Revelo. All rights reserved.
//

import UIKit
import FirebaseDatabase

class EnglishLevelViewController: UIViewController {

    private var dataSource: [CourseLevelModel] = []
    private var isDetailActive = true
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - override funcs
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Courses"
        loadCourses()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        isDetailActive = UserDefaults.standard.bool(forKey: "DETAIL_ACTIVE")
        tableView.reloadData()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "VocabularyViewController"{
            let vocabularyViewController = segue.destination as! VocabularyViewController
            vocabularyViewController.englishLevelSelected = sender as! CourseLevelModel
        }
    }

    
    //MARK: - private methods
    private func loadCourses(){
        let coursesReferences = Database.database().reference().child("course")
        coursesReferences.observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists(){
                if let jsonObject = snapshot.value as? [String:Any]{
                    for (key,value) in jsonObject{
                        //print(key)
                        //print(value)
                        let jsonData = try! JSONSerialization.data(withJSONObject: value, options: [])
                        let courseLevelModel = try! JSONDecoder().decode(CourseLevelModel.self, from: jsonData)
                        courseLevelModel.courseId = key
                        self.dataSource.append(courseLevelModel)
                    }
                    
                    self.dataSource = self.dataSource.sorted(by: { $0.name < $1.name })
                    
                    self.tableView.reloadData()
                }
            }
        }
    }

}

//MARK: - Extension UITableViewDelegate, UITableViewDataSource

extension EnglishLevelViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isDetailActive{
            let cell = tableView.dequeueReusableCell(withIdentifier: "EnglishLevelListTableViewCell") as! EnglishLevelListTableViewCell
            cell.course = dataSource[indexPath.row]
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "EnglishLabelWDTableViewCell") as! EnglishLabelWDTableViewCell
            cell.course = dataSource[indexPath.row]
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCourse = dataSource[indexPath.row]
        performSegue(withIdentifier: "VocabularyViewController", sender: selectedCourse)
    }
    
    
}
