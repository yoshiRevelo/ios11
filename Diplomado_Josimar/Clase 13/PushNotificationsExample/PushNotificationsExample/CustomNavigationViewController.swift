//
//  CustomNavigationViewController.swift
//  PushNotificationsExample
//
//  Created by MobileStudioMacBookAir02 on 07/07/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class CustomNavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(goToViewController(notification:)), name: NSNotification.Name(rawValue: "SHOW_VC"), object: nil)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK: - Private Methods
    
    @objc private func goToViewController(notification: Notification){
        if let identifier = notification.object as? String{
            if identifier == "r"{
                let redViewController = UIViewController()
                redViewController.view.backgroundColor = .red
                show(redViewController, sender: self)
            }else if identifier == "g" {
                let greenViewController = UIViewController()
                greenViewController.view.backgroundColor = .green
                show(greenViewController, sender: self)
            }
        }
    }
}
