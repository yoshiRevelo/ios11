//
//  DeepLinkHandler.swift
//  PushNotificationsExample
//
//  Created by MobileStudioMacBookAir02 on 07/07/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

class DeepLinkHandler {
    static let sharedInstance = DeepLinkHandler()
    
    func handleURL(url: URL){
        if let scheme = url.scheme, scheme == "apnsjosimar"{
            if let host = url.host{
                switch host{
                case "v":
                    print("Go to viewController")
                    let lastPath = url.lastPathComponent
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SHOW_VC"), object: lastPath)
                case "pd":
                    print("Go to pd")
                default:
                    print("Caso no manejado")
                }
            }
        }
    }
}
