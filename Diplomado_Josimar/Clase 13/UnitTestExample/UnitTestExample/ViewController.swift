//
//  ViewController.swift
//  UnitTestExample
//
//  Created by Ricardo López on 07/07/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var dataSource: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //populateDataSource()
        dataSource = populateDataSourceV2(num: 10)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func populateDataSourceV2(num: Int) -> [String] {
        var array: [String] = []
        for x in 0..<num {
            array.append("Hola \(x + 1)")
        }
        return array
    }
    
    func populateDataSource() {
        for x in 0..<10 {
            dataSource.append("Element \(x + 1)")
        }
    }

    func suma(numA: Int, numB: Int) -> Int {
        return numA + numB
    }

}

