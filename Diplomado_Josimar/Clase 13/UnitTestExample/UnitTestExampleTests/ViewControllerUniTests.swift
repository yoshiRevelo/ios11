//
//  ViewControllerUniTests.swift
//  UnitTestExampleTests
//
//  Created by Ricardo López on 07/07/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import XCTest
@testable import UnitTestExample
import Alamofire

class ViewControllerUniTests: XCTestCase {
    
    var viewController: ViewController!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        viewController = storyboard.instantiateInitialViewController() as! ViewController
        viewController = ViewController()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        viewController = nil
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    
    //MARK: - Unit Test
    func testSomeAsserts() {
        XCTAssert(true, "El resultado no es true")
        XCTAssertTrue("test" == "test", "El mensaje es diferente")
        XCTAssertFalse(2 == 1, "Los números son iguales")
    }
    
    func testSumResult() {
        let result = viewController.suma(numA: 5, numB: 7)
        XCTAssert(result == 12, "Función de suma está mal")
    }
    
    func testDataSourceLenght() {
        let dataSource = viewController.populateDataSourceV2(num: 13)
        XCTAssert(dataSource.count == 13, "Datasource corrupto")
    }
    
    //MARK: - Integration Tests
    
    func testAPICall(){
        
        let testExpectation = expectation(description: "Books API call")
        let booksURL = "https://www.mobilestudio.mx/iphone/parser/books.json"
        
        
        Alamofire.request(booksURL)
            .validate().responseJSON { (response) in
                switch response.result{
                case .success(let value):
                    print("Success")
                    if let jsonObject = value as? [String: Any]{
                        if let catalog = jsonObject["catalog"] as? [String: Any] {
                            if let booksArray = catalog["books"] as? [[String: Any]] {
                                XCTAssert(booksArray.count == 12, "Books array doesn't contain the right number of books")
                            }
                            else {
                                XCTFail("Invalid array for 'books'")
                            }
                        }else{
                            XCTFail("Invalid catalog key in JSON")
                        }
                    }else{
                        XCTFail("Invalid JSON")
                    }
                case .failure(let error):
                    //print("API Call Error: \(error.localizedDescription)")
                    XCTFail("API call failed with error: \(error.localizedDescription)")
                }
                testExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 3) { (error) in
            if let error = error {
                XCTFail("Expectations falied with error: \(error.localizedDescription)")
            }
        }
    }
}

















