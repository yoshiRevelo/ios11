//
//  ViewController.swift
//  PreprocessorsExample
//
//  Created by MobileStudioMacBookAir02 on 07/07/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        #if SANDBOX
        view.backgroundColor = UIColor.blue
        #else
        view.backgroundColor = UIColor.red
        #endif
        
        
        let completeUrl = "\(Constants.API.baseURL)"
        print(completeUrl)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

