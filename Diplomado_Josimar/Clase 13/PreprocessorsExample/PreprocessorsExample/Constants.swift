//
//  Constants.swift
//  PreprocessorsExample
//
//  Created by MobileStudioMacBookAir02 on 07/07/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

struct Constants {
    struct API {
        #if SANDBOX
        static let baseURL = "www.sandbox.com"
        #else
        static let baseURL = "www.production.com"
        #endif
    }
    
}
