//
//  SecondViewController.swift
//  ClosuresExample
//
//  Created by MobileStudio Laptop003 on 09/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

protocol SecondViewDelegate: class{
    func secondViewDidshare(text: String)
}

class SecondViewController: UIViewController{

    weak var delegate: SecondViewDelegate?
    var myUglyClosure: ((String)->Void)?
    private var myCoolClosure: ((String)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func set(myCoolClosure: @escaping((String)-> Void)){
        self.myCoolClosure = myCoolClosure
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK: - User interaction
    @IBAction func closureBottonPressed(_ sender: Any) {
        //myUglyClosure?("Texto enviado por closure")
        myCoolClosure?("Texto cool")
    }
    
    @IBAction func delegateButtonPressed(_ sender: Any) {
        delegate?.secondViewDidshare(text: "Texto enviado por delegate")
    }
}
