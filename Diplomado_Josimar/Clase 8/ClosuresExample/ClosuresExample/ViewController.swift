//
//  ViewController.swift
//  ClosuresExample
//
//  Created by MobileStudio Laptop003 on 09/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Definición implícita => "() ->"
        let simpleClosure = {
            print("Hola mundo closures")
            let x = 5, y = 6
            let result = x + y
            print("Result in  closure: \(result)")
            
        }
        
        simpleClosure()
        
        let simpleClosureWithDefinition: () -> Void = {
            print("Hola mundo closures dos")
        }
        
        simpleClosureWithDefinition()
        
        let closureWithParemeters = { (numA: Int, numB: Int) in
            let sum = numA + numB
            print("suma: \(sum)")
        }
        
        closureWithParemeters(5, 3)
        
        let closureWithParametersAndDefinition: (Int, Int) -> Void = { (numA:Int, numB:Int) in
            let sum = numA + numB
            print("suma2: \(sum)")
        }
        
        closureWithParametersAndDefinition(5, 12)
        
        let closureWithReturn: (Int, Int) -> Int = { (numA, numB) in
            let resta = numA - numB
            return resta
        }
        
        let resResult = closureWithReturn(5, 7)
        print("resta: \(resResult)")
        
        let myClosure: (String, Int) -> Void = { (name, age) in
            print("Hola soy \(name) y tengo \(age) años")
        }
        
        myClosure("Josimar", 25)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SecondViewController" {
            let secondViewController = segue.destination as! SecondViewController
            secondViewController.delegate = self
            
            //secondViewController.myUglyClosure = { (text) in
            //    print("Closure: \(text)")
            //}
            
            secondViewController.set(myCoolClosure: { [weak self] (text) in
                print("Cool closure: \(text)")
                self?.view.backgroundColor = .lightGray
            })
        }
    }
}

//MARK: - SecondViewDelegate

extension ViewController:SecondViewDelegate{
    func secondViewDidshare(text: String) {
        print("Delegate: \(text)")
    }
}
