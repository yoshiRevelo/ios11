//
//  ViewController.swift
//  CryptoSwiftExample
//
//  Created by Yoshi Revelo on 5/9/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import UIKit
import CryptoSwift
import RNCryptor

class ViewController: UIViewController {

    private var monthsArray: [String] = []
    private var yearsArray: [String] = []
    
    private var selectedMonth = ""
    private var selectedYear = ""
    private var pickerViewComponentsArray: [[String]] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        for i in 1 ... 12{
            if i < 10{
                monthsArray.append("0\(String(i))")
            }else{
                monthsArray.append(String(i))
            }
        }
        
        
        let date = Date()
        let years = DateFormatter()
        years.dateFormat = "yy"
        let formattedYear = years.string(from: date)
        let yearInt = Int(formattedYear)!
        
        selectedYear = formattedYear
        selectedMonth = "01"
        
        print(selectedYear)
        print(selectedMonth)
        
        for i in yearInt ... yearInt+10 {
            yearsArray.append(String(i))
        }
        
        print(yearsArray)
        print(monthsArray)
        
        pickerViewComponentsArray = [monthsArray, yearsArray]
        
        
        /*
        let user = "usuario"
        
        let key = "\(user)\(randomString(length: 9))"
        let iv = randomString(length: 16)
        
        
        print("\(key) && \(iv)")
        let input = "4242456789098765"
        let en = try! input.aesEncrypt(key: key, iv: iv)
        print("Codificado: \(en)")
        
        let dec = try! en.aesDecrypt(key: key, iv: iv)
        print("Decodificado \(dec)")*/
    }
    
    
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    
}

extension String{
    func aesEncrypt(key: String, iv: String) -> String{
        let data = self.data(using: String.Encoding.utf8)
        let encrypted = try! AES(key: key, iv: iv, padding: .pkcs7).encrypt([UInt8](data!))
        let encryptedData = Data(encrypted)
        return encryptedData.base64EncodedString()
        
    }
    
    func aesDecrypt(key: String, iv: String) -> String{
        let data = Data(base64Encoded: self)!
        let decrypted = try! AES(key: key, iv: iv, padding: .pkcs7).decrypt([UInt8](data))
        let decryptedData = Data(decrypted)
        return String(bytes: decryptedData.bytes, encoding: String.Encoding.utf8) ?? "No se pudo descifrar"
    }
}

extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerViewComponentsArray[component].count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerViewComponentsArray[component][row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch component {
        case 0:
            selectedMonth = pickerViewComponentsArray[component][row]
            print(selectedMonth)
        case 1:
            selectedYear = pickerViewComponentsArray[component][row]
            print(selectedYear)
        default:
            break
        }
    }    
    
}
