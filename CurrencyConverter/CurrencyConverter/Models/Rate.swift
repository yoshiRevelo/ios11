//
//  Rate.swift
//  CurrencyConverter
//
//  Created by Yoshi Revelo on 1/27/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import Foundation

class Rate: Codable{
    var name = ""
    var realname = ""
    var team = ""
    var firstappearance = ""
    var createdby = ""
    var publisher = ""
    var imageurl = ""
    var bio = ""
}

