//
//  rateResponseModel.swift
//  CurrencyConverter
//
//  Created by Yoshi Revelo on 1/27/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import Foundation

class rateResponseModel:Codable {
    var success = ""
    var timestamp = ""
    var base = ""
    var date = ""
    var rates: Rate
}
