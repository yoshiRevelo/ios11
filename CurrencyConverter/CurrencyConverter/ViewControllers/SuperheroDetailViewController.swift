//
//  SuperheroDetailViewController.swift
//  CurrencyConverter
//
//  Created by Yoshi Revelo on 1/27/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import UIKit

class SuperheroDetailViewController: UIViewController {

    @IBOutlet weak var heroImageView: UIImageView!
    @IBOutlet weak var heroNameLabel: UILabel!
    @IBOutlet weak var heroRealNameLabel: UILabel!
    @IBOutlet weak var heroTeamLabel: UILabel!
    @IBOutlet weak var heroFirstAppearanceLabel: UILabel!
    @IBOutlet weak var createdByLabel: UILabel!
    @IBOutlet weak var publisherLabel: UILabel!
    
    var superhero: Rate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSuperHeroViewController()
        // Do any additional setup after loading the view.
    }
    

    private func configureSuperHeroViewController(){
        
        
        if let urlheroImage = URL(string: superhero.imageurl){
            let heroImage = try? Data(contentsOf: urlheroImage)
            heroImageView.image = UIImage(data: heroImage!)
        }
        
        
        heroNameLabel.text = superhero.name
        heroRealNameLabel.text = superhero.realname
        heroTeamLabel.text = superhero.team
        heroFirstAppearanceLabel.text = superhero.firstappearance
        createdByLabel.text = superhero.createdby
        publisherLabel.text = superhero.publisher
    }

}
