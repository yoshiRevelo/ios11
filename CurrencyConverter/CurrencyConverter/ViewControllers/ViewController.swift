//
//  ViewController.swift
//  CurrencyConverter
//
//  Created by Yoshi Revelo on 1/25/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    private var datasource = [Rate]()
    
    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        loadCurrencies()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SuperheroDetailViewController"{
            let superheroDetailViewController = segue.destination as! SuperheroDetailViewController
            superheroDetailViewController.superhero = sender as! Rate
        }
    }
    
    //MARK: Private Methods
    private func loadCurrencies(){
        
        Alamofire.request(Constants.API.baseURL).validate()
            .responseData { (response) in
                //print("Request URL: \(response.request?.url?.absoluteURL)")
                switch(response.result){
                case .success(let value):
                    self.datasource.removeAll()
                    print("Data: \(value)")
                    do{
                        let superHeroesResponse = try JSONDecoder().decode([Rate].self, from: value)
                        self.datasource = superHeroesResponse
                        self.tableview.reloadData()
                        
                    }catch(let error){
                        print("Codable error: \(error.localizedDescription)")
                    }
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
                }
        }
    }
}

//MARK: Extension UITableViewDelegate, UITableViewDataSource
extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuperHeroTableViewCell") as! SuperHeroTableViewCell
        cell.superhero = datasource[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let superhero = self.datasource[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "SuperheroDetailViewController", sender: superhero)
    }
    
}

