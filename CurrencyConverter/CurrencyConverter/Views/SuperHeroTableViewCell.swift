//
//  SuperHeroTableViewCell.swift
//  CurrencyConverter
//
//  Created by Yoshi Revelo on 1/27/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import UIKit

class SuperHeroTableViewCell: UITableViewCell {

    @IBOutlet weak var superheroLabel: UILabel!
    
    @IBOutlet weak var teamLabel: UILabel!
    
    
    var superhero: Rate!{
        didSet{
            configureCell()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func configureCell(){
        superheroLabel.text = superhero.name
        teamLabel.text = superhero.team
    }

}
