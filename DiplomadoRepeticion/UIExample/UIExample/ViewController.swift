//
//  ViewController.swift
//  UIExample
//
//  Created by Yoshi Revelo on 4/5/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK: - Outlets
    
    @IBOutlet weak var holaMundoLabel: UILabel!
    
    @IBOutlet weak var imageViewLabel: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


    //MARK: - User Interaction
    
    @IBAction func leftButton(_ sender: Any) {
        holaMundoLabel.textAlignment = .left
    }
    
    @IBAction func centerButton(_ sender: Any) {
        holaMundoLabel.textAlignment = .center
    }
    
    @IBAction func rightButton(_ sender: Any) {
        holaMundoLabel.textAlignment = .right
    }
    
    @IBAction func redButton(_ sender: Any) {
        holaMundoLabel.textColor = .red
    }
    
    @IBAction func blackButton(_ sender: Any) {
        holaMundoLabel.textColor = .black
    }
    
    @IBAction func blueButton(_ sender: Any) {
        holaMundoLabel.textColor = .blue
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        imageViewLabel.alpha = CGFloat(sender.value)
    }
    
    
    
    
    
}

