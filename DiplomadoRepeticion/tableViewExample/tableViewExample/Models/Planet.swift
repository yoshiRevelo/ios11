//
//  Planet.swift
//  tableViewExample
//
//  Created by Yoshi Revelo on 4/7/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import Foundation

class Planet{
    
    var name = ""
    var planetDescription = ""
    
    init(name: String, planetDescription: String) {
        self.name = name
        self.planetDescription = planetDescription
    }
}
