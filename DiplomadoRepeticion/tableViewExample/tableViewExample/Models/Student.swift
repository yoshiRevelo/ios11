//
//  Student.swift
//  tableViewExample
//
//  Created by Yoshi Revelo on 4/7/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import Foundation

class Student{
    
    var name = ""
    var programmingLanguage = ""
    
    init(name: String, programmingLanguage: String) {
        self.name = name
        self.programmingLanguage = programmingLanguage
    }
}
