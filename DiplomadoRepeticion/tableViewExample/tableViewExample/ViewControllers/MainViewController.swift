//
//  MainViewController.swift
//  tableViewExample
//
//  Created by Yoshi Revelo on 4/7/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var editButtonPressed: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK: - Private vars
    private var planetsArray = [Planet]()
    private var studentsArray = [Student]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        //Planetas
        
        planetsArray.append(Planet(name: "Mercurio", planetDescription: "Descripción de Mercurio"))
        planetsArray.append(Planet(name: "Venus", planetDescription: "Descripción de Venus"))
        planetsArray.append(Planet(name: "Tierra", planetDescription: "Descripción de Tierra"))
        planetsArray.append(Planet(name: "Marte", planetDescription: "Descripción de Marte"))
        planetsArray.append(Planet(name: "Júpiter", planetDescription: "Descripción de Júpiter"))
        planetsArray.append(Planet(name: "Saturno", planetDescription: "Descripción de Saturno"))
        planetsArray.append(Planet(name: "Urano", planetDescription: "Descripción de Urano"))
        planetsArray.append(Planet(name: "Neptuno", planetDescription: "Descripción de Neptuno"))
        planetsArray.append(Planet(name: "Plutón", planetDescription: "Descripción de Plutón"))
        
        //Estudiantes
        
        studentsArray.append(Student(name: "Josimar", programmingLanguage: "Swift 5.0"))
        studentsArray.append(Student(name: "Willy", programmingLanguage: "Swift 4.2"))
        studentsArray.append(Student(name: "Revelo", programmingLanguage: "Swift 4.0"))
        studentsArray.append(Student(name: "Josimar", programmingLanguage: "Swift 3.0"))
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PlanetDetailViewController"{
            let planetDetailViewController = segue.destination as! PlanetDetailViewController
            planetDetailViewController.planet = (sender as! Planet)
        }else if segue.identifier == "StudentDetailViewController"{
            let studentDetailViewController = segue.destination as! StudentDetailViewController
            studentDetailViewController.student = (sender as! Student)
        }
    }

    
    
    //MARK: -  User Interaction
    
    @IBAction func editButton(_ sender: Any) {
        tableView.setEditing(!tableView.isEditing, animated: true)
        editButtonPressed.title = tableView.isEditing ? "Listo" : "Editar"
    }
    
}

//MARK: - Extension UITableViewDelegate, UITableViewDataSource

extension MainViewController: UITableViewDelegate, UITableViewDataSource{
    
    //Número de secciones de la tabla
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    //Número de filas de la tabla
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return planetsArray.count
        }else{
            return studentsArray.count
        }
    }
    
    
    //Configuración de la tabla
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "planetTableViewCell")!
            let planet = planetsArray[indexPath.row]
            
            cell.textLabel?.text = planet.name
            cell.accessoryType = .disclosureIndicator // muestra icono de la celda >
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "studentTableViewCell")!
            let student = studentsArray[indexPath.row]
            cell.textLabel?.text = student.name
            cell.detailTextLabel?.text = student.programmingLanguage
            
            cell.accessoryType = .checkmark
            
            return cell
        }
    }
    
    
    //Título de las secciones de la tabla
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return "Planetas"
        }else{
            return "Estudiantes"
        }
    }
    
    //Al seleccionar una fila de la tabla
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0{
            let planet = planetsArray[indexPath.row]
            performSegue(withIdentifier: "PlanetDetailViewController", sender: planet)
            tableView.deselectRow(at: indexPath, animated: true)
        }else{
            let student = studentsArray[indexPath.row]
            performSegue(withIdentifier: "StudentDetailViewController", sender: student)
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    
    //MARK: - Editar tabla
    
    //Nos dice cual sección se puede editar y cual no
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if  indexPath.section == 0 {
            return false
        }else{
            return true
        }
    }
    
    //Se llama cuando el usuario quiere hacer una acción de edición
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            //Se debe eliminar el elemento del arreglo y la celda de la tabla
            //deben ir juntos
            
            studentsArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    //Texto del boton eliminar
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Eliminar"
    }
    
    //MARK: - Mover la celda a otra posición
    
    //Este método nos dice si se puede mover una celda
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    //Este método nos dice a donde se quiere mover la celda
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let studentMove = studentsArray[sourceIndexPath.row]
        studentsArray.remove(at: sourceIndexPath.row)
        studentsArray.insert(studentMove, at: destinationIndexPath.row)
    }
    
    //Asegura que se mueva el elemento en la misma sección, si no se puede
    // lo devuelve
    func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        if sourceIndexPath.section == proposedDestinationIndexPath.section{
            return proposedDestinationIndexPath
        }else{
            return sourceIndexPath
        }
    }
}
