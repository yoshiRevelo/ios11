//
//  StudentDetailViewController.swift
//  tableViewExample
//
//  Created by Yoshi Revelo on 4/7/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import UIKit

class StudentDetailViewController: UIViewController {

    @IBOutlet weak var sutudentNameLabel: UILabel!
    
    @IBOutlet weak var programmingLanguageLabel: UILabel!
    
    var student: Student!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sutudentNameLabel.text = student.name
        programmingLanguageLabel.text = student.programmingLanguage
        // Do any additional setup after loading the view.
    }
}
