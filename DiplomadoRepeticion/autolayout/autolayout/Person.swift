//
//  Person.swift
//  autolayout
//
//  Created by Yoshi Revelo on 4/5/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import Foundation

class Person{
    
    var name = ""
    var age = 0
    
    init(name: String, age: Int) {
        self.name = name
        self.age = age
    }
    
    func saludo(){
        print("Hola soy \(name) y tengo \(age) años")
    }
}
