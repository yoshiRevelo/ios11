//
//  TodoItemDetailTableViewCell.swift
//  TodoListExample
//
//  Created by Yoshi Revelo on 4/7/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import UIKit

class TodoItemDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var colorIndicatorView: UIView!
    @IBOutlet weak var todoTitleLabel: UILabel!
    @IBOutlet weak var todoNotesLabel: UILabel!
    
    var todoItem: TodoItem!{
        didSet{
            configureCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //MARK: - private Methods
    
    private func configureCell(){
        todoTitleLabel.text = todoItem.title
        todoNotesLabel.text = todoItem.notes
        
        if todoItem.done{
            colorIndicatorView.backgroundColor = .green
            
            let attributesDict: [NSAttributedString.Key: Any] = [
                NSAttributedString.Key.strikethroughStyle : 1
            ]
            
            let attributedString = NSAttributedString(string: todoItem.title, attributes: attributesDict)
            
            todoTitleLabel.attributedText = attributedString
        }else{
            colorIndicatorView.backgroundColor = .red
            todoTitleLabel.text = todoItem.title
        }
    }
}
