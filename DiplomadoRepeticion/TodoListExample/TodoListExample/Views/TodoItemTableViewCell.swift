//
//  TodoItemTableViewCell.swift
//  TodoListExample
//
//  Created by Yoshi Revelo on 4/7/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import UIKit

class TodoItemTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var colorIndicatorView: UIView!
    
    var todoItem: TodoItem!{
        didSet{
            configureCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func configureCell(){
        titleLabel.text = todoItem.title
        if todoItem.done{
            colorIndicatorView.backgroundColor = .green
            
            //Crear attributes para la cadena de texto
            let attributesDict: [NSAttributedString.Key:Any] = [
                NSAttributedString.Key.strikethroughStyle : 1
            ]
            
            let attributedString = NSAttributedString(string: todoItem.title, attributes: attributesDict)
            titleLabel.attributedText = attributedString
        }else{
            colorIndicatorView.backgroundColor = .red
            titleLabel.text = todoItem.title
        }
    }


}
