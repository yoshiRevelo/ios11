//
//  TodoItem.swift
//  TodoListExample
//
//  Created by Yoshi Revelo on 4/7/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import Foundation

class TodoItem{
    var title = ""
    var notes: String?
    var done = false
    
    init(title: String, notes: String?, done: Bool) {
        self.title = title
        self.notes = notes
        self.done = done
    }
}
