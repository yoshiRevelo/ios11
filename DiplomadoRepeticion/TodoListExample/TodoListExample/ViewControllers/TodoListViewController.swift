//
//  TodoListViewController.swift
//  TodoListExample
//
//  Created by Yoshi Revelo on 4/7/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import UIKit

class TodoListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var dataSource = [TodoItem]()
    private var isDetailActive = true
    
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        //dummyData()
        
        if let todosDict = readJsonToDict(filename: "todos.json"){
            //print("Datos del diccionario \(todosDict)")
            
            //Arreglo de diccionarios [[String : Any]]
            if let todosArray = todosDict["todos"] as? [[String : Any]]{
                //print("Items \(todosArray.count)")
                
                //foreach
                
                for todoDict in todosArray{
                    if let title = todoDict["title"] as? String, let notes = todoDict["notes"] as? String, let done = todoDict["done"] as? Bool{
                        let todoItem = TodoItem(title: title, notes: notes, done: done)
                        dataSource.append(todoItem)
                    }
                }
            }
        }else{
            print("El archivo no existe")
        }
        // Do any additional setup after loading the view.
    }
    
    //MARK: - ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isDetailActive = UserDefaults.standard.bool(forKey: "DETAIL_ACTIVE")
        tableView.reloadData()
        print(isDetailActive)
    }
    
    //MARK: - Private Methods
    private func dummyData(){
        let todoDict : [String : Any] = [
            "title": "Estudiar Swift",
            "notes": "Buscar ejemplos de swift y entenderlos",
            "done": false]
        
        let todoDict2 : [String : Any] = [
            "title": "Comprar leche",
            "notes": "Deslactosada porque me hace daño la entera",
            "done" : false
        ]
        
        let todoDict3 : [String : Any] = [
            "title": "Lavar ropa",
            "notes": "Ya no tengo ropa limpia",
            "done" : true
        ]
        
        let todos = [
            "todos": [todoDict, todoDict2, todoDict3]
        ]
        
        writeDictToJson(dict: todos, filename: "todos.json")

    }
    
    private func writeDictToJson(dict: [String : Any], filename: String){
        
        //hay que serializarlo para obtener un data
        
        //se pueden serializar arreglos o diccionarios <-> JSON
        let data = try! JSONSerialization.data(withJSONObject: dict, options: [.prettyPrinted])
        
        //El archivo lo JSON lo debemos codificar en utf8
        let jsonString = String(data: data, encoding: .utf8)
        
        //Ruta del archivo donde vamos a escribir el archivo JSON
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        
        //ruta y nombre que le vamos a poner al archivo JSON
        let completePaths = paths[0].appending("/\(filename)")
        print(completePaths)
        
        do{
            try jsonString?.write(toFile: completePaths, atomically: true, encoding: .utf8)
        }catch let error{
            print("Error al escribir el archivo JSON \(error.localizedDescription)")
        }
    }

    private func readJsonToDict(filename: String) -> [String:Any]?{
        
        //Obtenemos el directorio documentos
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        
        //creamos la ruta completa del archivo
        let completePath = paths[0].appending("/\(filename)")
        
        //creamos la variable donde vamos a traer el archivo
        var dict: [String : Any]?
        
        do{
            let jsonString = try String(contentsOfFile: completePath, encoding: .utf8)
            //print("JSON String \(jsonString)")
            
            if let data = jsonString.data(using: .utf8){
                dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
            }
            
        }catch let error{
            print("Error al leer el archivo JSON \(error.localizedDescription)")
        }
        
        return dict
    }
}

extension TodoListViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isDetailActive{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TodoItemDetailTableViewCell") as! TodoItemDetailTableViewCell
            
            cell.todoItem = dataSource[indexPath.row]
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TodoItemTableViewCell") as! TodoItemTableViewCell
            
            cell.todoItem = dataSource[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
