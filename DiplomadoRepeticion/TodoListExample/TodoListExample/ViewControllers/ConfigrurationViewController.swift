//
//  ConfigrurationViewController.swift
//  TodoListExample
//
//  Created by Yoshi Revelo on 4/7/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import UIKit

class ConfigrurationViewController: UIViewController {
    
    @IBOutlet weak var detailSwitch: UISwitch!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        let isDetailActive = UserDefaults.standard.bool(forKey: "DETAIL_ACTIVE")
        detailSwitch.setOn(isDetailActive, animated: false)
        // Do any additional setup after loading the view.
    }
    

    //MARK: - User Interaction
    
    @IBAction func detailSwitchChanged(_ sender: UISwitch) {
        //print("cambia")
        
        UserDefaults.standard.set(detailSwitch.isOn, forKey: "DETAIL_ACTIVE")
        UserDefaults.standard.synchronize()
    }
    
}
