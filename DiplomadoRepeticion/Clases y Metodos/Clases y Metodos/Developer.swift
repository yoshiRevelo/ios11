//
//  Developer.swift
//  Clases y Metodos
//
//  Created by Yoshi Revelo on 4/5/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import Foundation

class Developer: Person {
    var lenguajeDeProgramacion = ""
    
    init(name: String, age: Int, lenguajeDeProgramacion: String){
        super.init(name: name, age: age)
        self.lenguajeDeProgramacion = lenguajeDeProgramacion
    }
    
    override func saludo() {
        super.saludo()
        print("Y ademas programo en \(lenguajeDeProgramacion)")
    }
}
