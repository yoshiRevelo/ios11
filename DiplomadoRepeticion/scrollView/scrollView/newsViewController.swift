//
//  newsViewController.swift
//  scrollView
//
//  Created by Yoshi Revelo on 4/6/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import UIKit

class newsViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var noteLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = "Crean mochila 'Spallbags' capaz de darle un descanso a la espalda"
        dateLabel.text = "28/04/2018"
        noteLabel.text = "CIUDAD DE MÉXICO \n\nPara los estudiantes de cualquier nivel educativo es menester llevar consigo una mochila o maleta en la cual se puedan depositar y transportar los diversos materiales de estudio y libros que se necesitan consultar. \n\nSin embargo, esa actividad, tan común entre los alumnos, puede traer consigo un sinfín de lesiones en los hombros, el cuello y la espalda, debido a las gran cantidad de peso que se concentra en una específica parte del cuerpo. CIUDAD DE MÉXICO \n\nPara los estudiantes de cualquier nivel educativo es menester llevar consigo una mochila o maleta en la cual se puedan depositar y transportar los diversos materiales de estudio y libros que se necesitan consultar. \n\nSin embargo, esa actividad, tan común entre los alumnos, puede traer consigo un sinfín de lesiones en los hombros, el cuello y la espalda, debido a las gran cantidad de peso que se concentra en una específica parte del cuerpo."
    }

}


/*
 
 1- Poner los contraints del ScrollView en 0 sin contraint to margins
 2- agregar un view con constraints a 0 y luego equalWidths con el View Principal
 3- agregar los elementos y sus constraints correspondientes sin olvidar el contraint de la parte baja para que se pueda calcular el scrollView
*/
