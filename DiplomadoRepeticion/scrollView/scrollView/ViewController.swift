//
//  ViewController.swift
//  scrollView
//
//  Created by Yoshi Revelo on 4/6/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var contentScrollView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //calculando el número de páginas en el scrollView
        
        let pages = contentScrollView.frame.size.width/view.frame.width
        pageControl.numberOfPages = Int(pages)
    }
}

extension ViewController: UIScrollViewDelegate{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentPage = scrollView.contentOffset.x/view.frame.width
        pageControl.currentPage = Int(currentPage)
    }
}

/* ScrollView con pageControl
ScrollView - area donde me puedo desplazar

constraints para el scroll

estas constraints se llaman constraint inset- àrea de rebote

 1- crear un scroll view y sus constraint -> 0 (quitar constraints to margin) -> darle paging enabled a las propiedades del scrollView

 2- Crear un view -> llamarlo ContentView y sus constraints -> 0

3- crear dos views dentro del content view

 4- a cada view ponerle constraints de 0

5- darle control clic a cada view y arrastrarlo al view principal -> seleccionar equal height y equal width

6- agregar un PageControl al nivel e ScrollView pero abajo del scrollView

7- agregar outlets pageControl y contentView

8- agregar el delegate del scrollView
*/
