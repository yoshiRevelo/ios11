//
//  ViewController.swift
//  basicNavigation
//
//  Created by Yoshi Revelo on 4/6/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func programmaticallySegueButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "blueViewController", sender: nil)
    }
    
    @IBAction func idViewControllerButtonPressed(_ sender: Any) {
        if let grayViewController = storyboard?.instantiateViewController(withIdentifier: "grayViewController"){
            navigationController?.show(grayViewController, sender: self)
        }
    }
    
    
}

