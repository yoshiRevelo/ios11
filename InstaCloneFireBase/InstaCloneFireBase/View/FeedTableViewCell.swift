//
//  FeedTableViewCell.swift
//  InstaCloneFireBase
//
//  Created by Yoshi Revelo on 2/8/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import UIKit

class FeedTableViewCell: UITableViewCell {

    //MARK: - Outlets
    
    
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var feedImageView: UIImageView!
    
    @IBOutlet weak var commentTextView: UITextView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
