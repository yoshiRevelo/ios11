//
//  SignInViewController.swift
//  InstaCloneFireBase
//
//  Created by Yoshi Revelo on 2/4/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import UIKit
import Firebase


class SignInViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - User interaction

    @IBAction func signInButtonPressed(_ sender: Any) {
        
        if emailTextField.text != "" && passwordTextField.text != "" {
            Auth.auth().signIn(withEmail: emailTextField.text!, password: passwordTextField.text!) { (success, error) in
                if error != nil{
                    let alert = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                }else{
                    
                    self.login()
                    //self.performSegue(withIdentifier: "SignInViewController", sender: nil)
                }
            }
        }else{
            let alert = UIAlertController(title: "Error", message: "Ingrese el usuario o contraseña", preferredStyle: .alert)
            
            let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alert.addAction(alertAction)
            
            present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func signUpButtonPresse(_ sender: Any) {
            let userAlertController = UIAlertController(title: "Nuevo usuario", message: "Introducir datos", preferredStyle: .alert)
            
            let saveAlertAction = UIAlertAction(title: "Guardar", style: .default) { (action) in
                let emailField = userAlertController.textFields![0]
                let passwordField = userAlertController.textFields![1]
        
                Auth.auth().createUser(withEmail: emailField.text!, password: passwordField.text!, completion: { (success, error) in
                    if error != nil{
                        let alert = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                        
                        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                        
                        alert.addAction(alertAction)
                        
                        self.present(alert ,animated: true, completion: nil)
                    }else{
                        self.login()
                    }
                })
        }
            
            let userCancelAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
            
            userAlertController.addTextField { (emailField) in
                emailField.placeholder = "email"
            }
            
            userAlertController.addTextField { (passwordField) in
                passwordField.placeholder = "password"
                passwordField.isSecureTextEntry = true
            }
            
            userAlertController.addAction(saveAlertAction)
            userAlertController.addAction(userCancelAction)
            
            present(userAlertController, animated: true, completion: nil)
    }
    
    private func login(){
        UserDefaults.standard.set(self.emailTextField.text, forKey: "user")
        UserDefaults.standard.synchronize()
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.rememberLogin()
    }
}
