//
//  ViewController.swift
//  InstaCloneFireBase
//
//  Created by Yoshi Revelo on 2/4/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage

class FeedViewController: UIViewController {

    private var userEmailArray = [String]()
    private var postCommentArray = [String]()
    private var postImageArray = [String]()
    private var refreshContol = UIRefreshControl()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        refreshContol.addTarget(self, action: #selector(getDataFromFirebase), for: .valueChanged)
        tableView.addSubview(refreshContol)
        getDataFromFirebase()
    }
    
    //MARK: - Private Methods
    @objc private func getDataFromFirebase(){
       
        userEmailArray.removeAll()
        postCommentArray.removeAll()
        postImageArray.removeAll()
        Database.database().reference().child("users").observe(DataEventType.childAdded) { (snapShot) in
            
            let values = snapShot.value! as! NSDictionary
            
            let post = values["post"] as! NSDictionary
            
            let postIDs = post.allKeys
            
            for id in postIDs {
                let singlePost = post[id] as! NSDictionary
                
                self.userEmailArray.append(singlePost["postedBy"] as! String)
                self.postCommentArray.append(singlePost["postText"] as! String)
                self.postImageArray.append(singlePost["image"] as! String)
            }
            self.tableView.reloadData()
            self.refreshContol.endRefreshing()
        }
    }

    //MARK: - User interaction
    
    @IBAction func logOutButtonPressed(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "user")
        UserDefaults.standard.synchronize()
        
        let signIn = storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.window?.rootViewController = signIn
        
        delegate.rememberLogin()
    }
    
}

extension FeedViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userEmailArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedTableViewCell", for: indexPath) as! FeedTableViewCell
        
        cell.userNameLabel.text = userEmailArray[indexPath.row]
        cell.commentTextView.text = postImageArray[indexPath.row]
        cell.feedImageView.sd_setImage(with: URL(string: postImageArray[indexPath.row]), completed: nil)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
}

