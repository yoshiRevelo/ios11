//
//  UploadViewController.swift
//  InstaCloneFireBase
//
//  Created by Yoshi Revelo on 2/4/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import UIKit
import Firebase

class UploadViewController: UIViewController {

    private var uniqueID = NSUUID().uuidString
    
    @IBOutlet weak var postImageView: UIImageView!
    
    @IBOutlet weak var commentPostTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //activar la interacción con el usuario
        postImageView.isUserInteractionEnabled = true
        
        //declarar el tap regognizer y el selector
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(UploadViewController.choosePhoto))
        
        //agregar el gestureRecognizer
        postImageView.addGestureRecognizer(recognizer)
    }
    
    
    //funcion que selecciona la imagen al dar tap sobre ella
    @objc private func choosePhoto(){
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func postButtonPressed(_ sender: Any) {
        let mediaFolder = Storage.storage().reference().child("media")
        
        if let data = postImageView.image?.jpegData(compressionQuality: 0.5){
            mediaFolder.child("\(uniqueID).jpg").putData(data, metadata: nil) { (metadata, error) in
                if error != nil{
                    let alert = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    
                    alert.addAction(alertAction)
                    
                    self.present(alert, animated: true, completion: nil)
                }else{
                    mediaFolder.child("\(self.uniqueID).jpg").downloadURL(completion: { (url, error) in
                        if error != nil {
                            let alert = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                            
                            let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                            
                            alert.addAction(alertAction)
                            
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            let imageURL = url?.absoluteString
                            
                            let post: [String: Any] = ["image": imageURL!, "postedBy": Auth.auth().currentUser?.email!, "uuid": self.uniqueID, "postText": self.commentPostTextView.text] as [String : Any]
                            
                            Database.database().reference().child("users").child((Auth.auth().currentUser?.uid)!).child("post").childByAutoId().setValue(post)
                            
                            self.postImageView.image = UIImage(named: "placeholder.png")
                            self.commentPostTextView.text = ""
                            self.tabBarController?.selectedIndex = 0
                        }
                    })
                }
            }
        }
    }

}


extension UploadViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        //imformación que contiene el imageView
        postImageView.image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        
        //regresar a la pantalla de nuestra app
        self.dismiss(animated: true, completion: nil )
    }
}
