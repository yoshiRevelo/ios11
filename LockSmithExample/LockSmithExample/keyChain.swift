//
//  keyChain.swift
//  LockSmithExample
//
//  Created by Yoshi Revelo on 5/10/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import Foundation
import Locksmith


struct keyChain: ReadableSecureStorable,
    CreateableSecureStorable,
    DeleteableSecureStorable,
GenericPasswordSecureStorable {
    var data: [String : Any]
    
    let username: String
    let password: String
    
    let service = "Login"
    var account: String { return username }
}


