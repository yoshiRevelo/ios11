//
//  ViewController.swift
//  LockSmithExample
//
//  Created by Yoshi Revelo on 5/9/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import UIKit
import Locksmith

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        var dic1 : [String : Any] = [:]
        do{
            dic1["card1"] = "1234"
            try Locksmith.updateData(data: dic1, forUserAccount: "myUserAccount", inService: "my_password")
            //try Locksmith.updateData(data: dic1, forUserAccount: "myUserAccount")
            
            //try Locksmith.updateData(data: dic1, forUserAccount: "myUserAccount")
            
            //try Locksmith.deleteDataForUserAccount(userAccount: "myUserAccount")
            
        }catch let error{
            print(error.localizedDescription)
        }
        
        do{
           
            dic1["card2"] = "545343"
            try Locksmith.updateData(data: dic1, forUserAccount: "myUserAccount", inService: "my_password")

            
            dic1["card3"] = "545343"
            try Locksmith.updateData(data: dic1, forUserAccount: "myUserAccount", inService: "my_password")

            
        }catch let error{
            print(error.localizedDescription)
        }
        
        
        var dictionary = Locksmith.loadDataForUserAccount(userAccount: "myUserAccount", inService: "")
        
        print(dictionary)
        
        dictionary = Locksmith.loadDataForUserAccount(userAccount: "myUserAccount", inService: "my_password")
        
        print(dictionary)
        
        try! Locksmith.deleteDataForUserAccount(userAccount: "myUserAccount", inService: "my_password")
        
        
        
        /*
        let account = keyChain(data: ["password" : "dasdasda",
            "algo" : "dfsfsdf"], username: "_matthewpalmer", password: "my_password")
        
        //CreateableSecureStorable lets us create the account in the keychain
        //try! account.createInSecureStore()
        
        // ReadableSecureStorable lets us read the account from the keychain
        let result = account.readFromSecureStore()
        print(result)
        
        
        // DeleteableSecureStorable lets us delete the account from the keychain
        try! account.deleteFromSecureStore()
 */
    
    }
    
    

    

}

