//
//  LocationsViewController.swift
//  travelMapBook
//
//  Created by Yoshi Revelo on 12/14/18.
//  Copyright © 2018 Yoshi Revelo. All rights reserved.
//

import UIKit
import CoreData

class LocationsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var titleArray = [String]()
    var subtitleArray = [String]()
    var latitudeArray = [Double]()
    var longitudeArray = [Double]()
    
    
    var choosenTitle = ""
    var choosenSubtitle = ""
    var choosenLatitude:Double = 0
    var choosenLongitude:Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(LocationsViewController.fetchData), name: NSNotification.Name(rawValue: "newLocationCreated"), object: nil)
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LocationsViewController"{
            let locationsViewController = segue.destination as! ViewController
            locationsViewController.transmittedTitle = choosenTitle
            locationsViewController.transmittedSubtitle = choosenSubtitle
            locationsViewController.transmittedLatitude = choosenLatitude
            locationsViewController.transmittedLongitude = choosenLongitude
        }
    }
    
    //MARK: - User Interaction
    @IBAction func addButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "LocationsViewController", sender: nil)
    }
    
    @objc private func fetchData(){
        let context = CoreDataManager.sharedInstance.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Locations")
        request.returnsObjectsAsFaults = false
        
        do{
            let results = try context.fetch(request)
            
            if results.count > 0{
                
                titleArray.removeAll(keepingCapacity: false)
                subtitleArray.removeAll(keepingCapacity: false)
                latitudeArray.removeAll(keepingCapacity: false)
                longitudeArray.removeAll(keepingCapacity: false)
                
                for result in results as! [NSManagedObject]{
                    if let title = result.value(forKey: "title") as? String{
                        titleArray.append(title)
                    }
                    
                    if let subtitle = result.value(forKey: "subtitle") as? String{
                        subtitleArray.append(subtitle)
                    }
                    
                    if let latitude = result.value(forKey: "latitude") as? Double{
                        latitudeArray.append(latitude)
                    }
                    
                    if let longitude = result.value(forKey: "longitude") as? Double{
                        longitudeArray.append(longitude)
                    }
                    
                    self.tableView.reloadData()
                }
            }
        }catch let error{
            print("Error reading \(error.localizedDescription)")
        }
    }
}

extension LocationsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationsViewController")
        cell?.textLabel?.text = titleArray[indexPath.row]
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        choosenTitle = titleArray[indexPath.row]
        choosenSubtitle = subtitleArray[indexPath.row]
        choosenLatitude = latitudeArray[indexPath.row]
        choosenLongitude = longitudeArray[indexPath.row]
 
        performSegue(withIdentifier: "LocationsViewController", sender: nil)
    }
    
}
