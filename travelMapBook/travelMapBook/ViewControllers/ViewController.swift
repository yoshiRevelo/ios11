//
//  ViewController.swift
//  travelMapBook
//
//  Created by Yoshi Revelo on 12/14/18.
//  Copyright © 2018 Yoshi Revelo. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var descriptionTextView: UITextField!
    
    private var locationManager = CLLocationManager()
    
    private var choosenlatitude:Double = 0
    private var choosenLongitude:Double = 0
    
    var transmittedTitle = ""
    var transmittedSubtitle = ""
    var transmittedLatitude: Double = 0
    var transmittedLongitude: Double = 0
    
    var requestCLLocation = CLLocation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        //create Gesture Recognition
        
        let recognizer = UILongPressGestureRecognizer(target: self, action: #selector(ViewController.chooseLocation(gestureRecognizer:)))
        
        recognizer.minimumPressDuration = 3
        
        mapView.addGestureRecognizer(recognizer)
        
        if transmittedTitle != ""{
            let annotation = MKPointAnnotation()
            let coordinate = CLLocationCoordinate2D(latitude: transmittedLatitude, longitude: transmittedLongitude)
            
            annotation.coordinate = coordinate
            annotation.title = transmittedTitle
            annotation.subtitle = transmittedSubtitle
            
            mapView.addAnnotation(annotation)
            
            nameText.text = transmittedTitle
            descriptionTextView.text = transmittedSubtitle
        }
    }
    
    @IBAction func okButtonPressed(_ sender: Any) {
        //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = CoreDataManager.sharedInstance.persistentContainer.viewContext
        
        let newLocation = NSEntityDescription.insertNewObject(forEntityName: "Locations", into: context)
        
        newLocation.setValue(nameText.text, forKey: "title")
        newLocation.setValue(descriptionTextView.text, forKey: "subtitle")
        newLocation.setValue(choosenlatitude, forKey: "latitude")
        newLocation.setValue(choosenLongitude, forKey: "longitude")
        
        CoreDataManager.sharedInstance.saveContext()
        print("The location has been saved!")
        
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newLocationCreated"), object: nil)
        navigationController?.popViewController(animated: true)
    }
    
    
    
    //MARK: - Private functions
    
    @objc private func chooseLocation(gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .began{
            let touchedPoint = gestureRecognizer.location(in: mapView)
            let choosenCoordinates = mapView.convert(touchedPoint, toCoordinateFrom: mapView)
            
            choosenlatitude = choosenCoordinates.latitude
            choosenLongitude = choosenCoordinates.longitude
            
            let annotation = MKPointAnnotation()
            annotation.coordinate = choosenCoordinates
            annotation.title = nameText.text
            annotation.subtitle = descriptionTextView.text
            
            mapView.addAnnotation(annotation)
        }
    }
}


//MARK: - Extension - MKMapViewDelegate, CLLocationManagerDelegate
extension ViewController: MKMapViewDelegate, CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = CLLocationCoordinate2DMake(locations[0].coordinate.latitude, locations[0].coordinate.longitude)
        
        //normal zoom level 0.05
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        
        let region = MKCoordinateRegion(center: location, span: span)
        
        mapView.setRegion(region, animated: true)
    }
    
    
    //For trace a route in anyMaps app
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation{
            return nil
        }
        
        let reuseID = "myAnnotation"
        
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseID) as? MKPinAnnotationView
        
        if pinView == nil{
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseID)
            
            pinView?.canShowCallout = true
            pinView?.pinTintColor = .red
            let button = UIButton(type: .detailDisclosure)
            pinView?.rightCalloutAccessoryView = button
        }else{
            pinView?.annotation = annotation
        }
        
        return pinView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if transmittedLatitude != 0, transmittedLongitude != 0{
            requestCLLocation = CLLocation(latitude: transmittedLatitude, longitude: transmittedLongitude)
        }
        
        CLGeocoder().reverseGeocodeLocation(requestCLLocation) { (placemarks, error) in
            if let placemark = placemarks {
                if placemark.count > 0{
                    let newPlaceMark = MKPlacemark(placemark: placemark[0])
                    let item = MKMapItem(placemark: newPlaceMark)
                    item.name = self.transmittedTitle
                    
                    let launchedOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
                    item.openInMaps(launchOptions: launchedOptions)
                }
            }
        }
    }
    
}

extension ViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

