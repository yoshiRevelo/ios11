//
//  ViewController.swift
//  LocalNotifications
//
//  Created by Yoshi Revelo on 1/30/19.
//  Copyright © 2019 Yoshi Revelo. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController {

    private var rememberTime = 10.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UNUserNotificationCenter.current().delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }


    @IBAction func LocalNotificationButtonPressed(_ sender: Any) {
        sendLocalNotification()
    }
    
    //MARK: private methods
    private func sendLocalNotification(){
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: rememberTime, repeats: false)
        
        let content = UNMutableNotificationContent()
        content.title = "Local Notification"
        content.subtitle = "Learning about it"
        content.body = "I'm learning about local notifications!!!"
        content.sound = UNNotificationSound.default
        
        if let path = Bundle.main.path(forResource: "zelda", ofType: "jpg"){
            let url = URL(fileURLWithPath: path)
            do{
                let attachment = try UNNotificationAttachment(identifier: "zelda", url: url, options: nil)
                content.attachments = [attachment]
            }catch(let error){
                print("Ocurrió un error al cargar la imagen \(error)")
            }
        }
        //1 Accion recordar
        let rememberAction = UNNotificationAction(identifier: "rememberAction", title: "Recordar más tarde", options: [])
        
        //2 Creamos la acción eliminar
        let deleteAction = UNNotificationAction(identifier: "deleteAction", title: "Eliminar", options: [])
        
        //3 Crear categoria que agrupa las acciones
        let category = UNNotificationCategory(identifier: "zeldaCategory", actions: [rememberAction, deleteAction], intentIdentifiers: [], options: [])
        
        //4 Añadir la categoría al centro de notificaciones
        UNUserNotificationCenter.current().setNotificationCategories([category])
        
        content.categoryIdentifier = "zeldaCategory"
        
        let request = UNNotificationRequest(identifier: "LocalNotification", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().add(request) { (error) in
            if let error = error{
                print("Ha habido un error \(error)")
            }
        }
    }
}

extension ViewController: UNUserNotificationCenterDelegate{
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        //1 RememberAction
        if response.actionIdentifier == "rememberAction" {
            rememberTime = 15.0
            sendLocalNotification()
        }
        //2 eliminar notificacion
        else if response.actionIdentifier == "deleteAction" {
            UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: ["zeldaNotification"])
        }
    }
}

